class AddSubServiceOrderIdToMaterial < ActiveRecord::Migration
  def change
    add_column :materials, :sub_service_order_id, :integer
  end
end
