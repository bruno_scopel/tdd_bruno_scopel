class AddServiceDescriptionToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :service_description, :text
  end
end
