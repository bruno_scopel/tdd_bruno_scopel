class AddSoIdToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :so_id, :integer
  end
end
