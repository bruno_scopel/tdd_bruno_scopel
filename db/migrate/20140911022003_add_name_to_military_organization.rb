class AddNameToMilitaryOrganization < ActiveRecord::Migration
  def change
    add_column :military_organizations, :name, :string
  end
end
