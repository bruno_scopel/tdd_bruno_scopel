class AddMaterialDescriptionToSo < ActiveRecord::Migration
  def change
    add_column :sos, :material_description, :string
  end
end
