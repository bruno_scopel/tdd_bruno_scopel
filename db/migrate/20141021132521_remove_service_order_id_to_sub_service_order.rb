class RemoveServiceOrderIdToSubServiceOrder < ActiveRecord::Migration
  def change
    remove_column :sub_service_orders, :service_order_id, :integer
  end
end
