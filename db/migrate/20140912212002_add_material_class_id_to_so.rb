class AddMaterialClassIdToSo < ActiveRecord::Migration
  def change
    add_column :sos, :material_class_id, :integer
  end
end
