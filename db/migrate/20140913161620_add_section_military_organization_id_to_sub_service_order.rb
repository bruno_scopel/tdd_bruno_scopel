class AddSectionMilitaryOrganizationIdToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :section_military_organization_id, :integer
  end
end
