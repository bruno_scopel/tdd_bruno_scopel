class AddAcronymToMaterialClass < ActiveRecord::Migration
  def change
    add_column :material_classes, :acronym, :string
  end
end
