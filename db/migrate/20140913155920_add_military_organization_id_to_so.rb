class AddMilitaryOrganizationIdToSo < ActiveRecord::Migration
  def change
    add_column :sos, :military_organization_id, :integer
  end
end
