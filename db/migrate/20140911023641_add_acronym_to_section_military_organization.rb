class AddAcronymToSectionMilitaryOrganization < ActiveRecord::Migration
  def change
    add_column :section_military_organizations, :acronym, :string
  end
end
