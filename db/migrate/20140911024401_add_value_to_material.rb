class AddValueToMaterial < ActiveRecord::Migration
  def change
    add_column :materials, :value, :float
  end
end
