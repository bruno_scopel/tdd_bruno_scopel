class AddNameToMaterialClass < ActiveRecord::Migration
  def change
    add_column :material_classes, :name, :string
  end
end
