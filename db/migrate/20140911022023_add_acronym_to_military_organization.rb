class AddAcronymToMilitaryOrganization < ActiveRecord::Migration
  def change
    add_column :military_organizations, :acronym, :string
  end
end
