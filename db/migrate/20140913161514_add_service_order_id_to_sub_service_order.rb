class AddServiceOrderIdToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :service_order_id, :integer
  end
end
