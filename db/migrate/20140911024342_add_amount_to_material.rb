class AddAmountToMaterial < ActiveRecord::Migration
  def change
    add_column :materials, :amount, :float
  end
end
