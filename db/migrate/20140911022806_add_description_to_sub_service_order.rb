class AddDescriptionToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :description, :text
  end
end
