class AddNameToSectionMilitaryOrganization < ActiveRecord::Migration
  def change
    add_column :section_military_organizations, :name, :string
  end
end
