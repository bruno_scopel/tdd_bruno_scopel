class AddTechnicalToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :technical, :string
  end
end
