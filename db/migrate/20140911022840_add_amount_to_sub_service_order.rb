class AddAmountToSubServiceOrder < ActiveRecord::Migration
  def change
    add_column :sub_service_orders, :amount, :integer
  end
end
