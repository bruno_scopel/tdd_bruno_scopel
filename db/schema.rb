# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141021133105) do

  create_table "material_classes", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "acronym"
  end

  create_table "materials", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.float    "amount"
    t.float    "value"
    t.string   "unit"
    t.integer  "sub_service_order_id"
  end

  create_table "military_organizations", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "acronym"
  end

  create_table "section_military_organizations", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "acronym"
  end

  create_table "sos", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "material_description"
    t.integer  "guide_number"
    t.string   "sender_name"
    t.string   "sender_phone"
    t.integer  "total_amount"
    t.integer  "material_class_id"
    t.integer  "military_organization_id"
  end

  create_table "sub_service_orders", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.integer  "amount"
    t.text     "service_description"
    t.string   "technical"
    t.integer  "section_military_organization_id"
    t.integer  "so_id"
  end

  create_table "tests", force: true do |t|
    t.string   "attr1"
    t.string   "attr2"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
