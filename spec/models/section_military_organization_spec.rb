require 'spec_helper'
require 'rails_helper'

RSpec.describe SectionMilitaryOrganization, :type => :model do

  fixtures :section_military_organizations

  it "section military organization should be saved" do
      section = SectionMilitaryOrganization.new
      assert section.save(validate: false), "It was not possible to save a section military organization"
  end

  #NAME
  
  it "section military organization should have name" do
      section = SectionMilitaryOrganization.new(:name => "Informatica")
      assert section.save(validate: false), "It was not possible to insert a name"
  end

  it "Section Military Organization Name not should be nil" do 
    miliary_org = section_military_organizations(:name_nil).should_not be_valid 
  end

  it "Section Military Organization Name can not be less than 2 characters" do 
    miliary_org = section_military_organizations(:short_name).should_not be_valid 
  end
  it "Section Military Organization Name can not be more than 30 characters" do 
    miliary_org = section_military_organizations(:long_name).should_not be_valid
  end

  #ACRONYM
  
  it "section military organization should have acronym" do
      section = SectionMilitaryOrganization.new(:acronym => "Sinfo")
      assert section.save(validate: false), "It was not possible to insert a acronym"
  end

  it "Section Military Organization Acronym not should be nil" do 
    miliary_org = section_military_organizations(:acronym_nil).should_not be_valid
  end
  
  it "Section Military Organization Acronym can not be less than 2 characters" do 
    miliary_org = section_military_organizations(:short_acronym).should_not be_valid 
  end
  
  it "Section Military Organization Acronym can not be more than 20 characters" do 
    miliary_org = section_military_organizations(:long_acronym).should_not be_valid
  end
end
