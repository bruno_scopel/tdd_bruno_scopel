require 'spec_helper'
require 'rails_helper'

RSpec.describe SubServiceOrder, :type => :model do

  fixtures :sub_service_orders

  it "sub service order should be saved" do
      sub_service_order = SubServiceOrder.new
      assert sub_service_order.save(validate: false), "It was not possible to save a sub service order"
  end

  #DESCRIPTION
  
  it "sub service order should have description" do
      sub_service_order = SubServiceOrder.new(:description => "material x")
      assert sub_service_order.save(validate: false), "It was not possible to insert a description"
  end

  it "Sub Service Order Description not should be nil" do 
    sub_service_order = sub_service_orders(:description_nil).should_not be_valid 
  end

  it "Sub Service Order Description can not be less than 2 characters" do 
    sub_service_order = sub_service_orders(:description_short).should_not be_valid 
  end
  it "Sub Service Order Description can not be more than 500 characters" do 
    sub_service_order = sub_service_orders(:description_long).should_not be_valid
  end

  #AMOUNT

  it "sub service order should have amount" do
    sub_service_order = SubServiceOrder.new(:amount => 1)
    assert sub_service_order.save(validate: false), "It was not possible to insert a amount"
  end

  it "Sub Service Order Sender Phone not should be nil" do 
    sub_service_order = sub_service_orders(:amount_nil).should_not be_valid 
  end

  #SERVICE_DESCRIPTION

  it "sub service order should have service description" do
    sub_service_order = SubServiceOrder.new(:service_description => "material x")
    assert sub_service_order.save(validate: false), "It was not possible to insert a service description"
  end

  it "Sub Service Order Service Description not should be nil" do 
    sub_service_order = sub_service_orders(:service_description_nil).should_not be_valid 
  end

  it "Sub Service Order Service Description can not be less than 2 characters" do 
    sub_service_order = sub_service_orders(:service_description_short).should_not be_valid 
  end

  it "Sub Service Order Service Description can not be more than 500 characters" do 
    sub_service_order = sub_service_orders(:service_description_long).should_not be_valid
  end

  #TECHNICAL

  it "sub service order should have technical" do
    sub_service_order = SubServiceOrder.new(:description => "sgt ciclano")
    assert sub_service_order.save(validate: false), "It was not possible to insert a technical"
  end

  it "Sub Service Order Technical not should be nil" do 
    sub_service_order = sub_service_orders(:technical_nil).should_not be_valid 
  end

  it "Sub Service Order Technical can not be less than 2 characters" do 
    sub_service_order = sub_service_orders(:technical_short).should_not be_valid 
  end
  it " Sub Service Order Technical can not be more than 30 characters" do 
    sub_service_order = sub_service_orders(:technical_long).should_not be_valid
  end

  #RELACIONAMENTOS  
  
  it "Service Order ID not should be nil" do 
    sub_service_order = sub_service_orders(:service_order_id_nil).should_not be_valid 
  end

  it "Section Military Organization ID not should be nil" do 
    sub_service_order = sub_service_orders(:section_military_organization_id_nil).should_not be_valid 
  end
   
end
