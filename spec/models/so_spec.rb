require 'spec_helper'
require 'rails_helper'

RSpec.describe So, :type => :model do

  fixtures :sos

  it "service order should be saved" do
      service_order = So.new
      assert service_order.save(validate: false), "It was not possible to save a service order"
  end

  #MATERIAL_DESCRIPTION
  
  it "service order should have material description" do
      service_order = So.new(:material_description => "My material")
      assert service_order.save(validate: false), "It was not possible to insert a material_description"
  end

  it "Service Order Material Description not should be nil" do 
    service_order = sos(:material_description_nil).should_not be_valid 
  end

  it "Service Order Material Description can not be less than 2 characters" do 
    service_order = sos(:material_description_short).should_not be_valid 
  end

  it "Service Order Material Description can not be more than 100 characters" do 
    service_order = sos(:material_description_long).should_not be_valid
  end



  #GUIDE_NUMBER

  it "service order should have guide number" do
    service_order = So.new(:guide_number => "2014001")
    assert service_order.save(validate: false), "It was not possible to insert a guide_number"
  end

  it "Service Order Guide Number not should be nil" do 
    service_order = sos(:guide_number_nil).should_not be_valid 
  end

  it "Service Order Guide Number can not be less than 2 characters" do 
    service_order = sos(:guide_number_short).should_not be_valid 
  end
  it "Service Order Guide Number can not be more than 10 characters" do 
    service_order = sos(:guide_number_long).should_not be_valid
  end


  #SENDER_NAME

  it "service order should have sender name" do
    service_order = So.new(:sender_name => "Cb Fulano")
    assert service_order.save(validate: false), "It was not possible to insert a sender name"
  end

  it "Service Order Sender Name not should be nil" do 
    service_order = sos(:sender_name_nil).should_not be_valid 
  end

  it "Service Order Sender Name can not be less than 2 characters" do 
    service_order = sos(:sender_name_short).should_not be_valid 
  end

  it "Service Order Sender Name can not be more than 20 characters" do 
    service_order = sos(:sender_name_long).should_not be_valid
  end


  #SENDER_PHONE

  it "service order should have sender phone" do
    service_order = So.new(:sender_phone => "(xx)xxxx-xxxx")
    assert service_order.save(validate: false), "It was not possible to insert a sender phone"
  end

  it "Service Order Sender Phone not should be nil" do 
    service_order = sos(:sender_phone_nil).should_not be_valid 
  end

  it "Service Order Sender Phone can not be less than 13 characters" do 
    service_order = sos(:sender_phone_short).should_not be_valid 
  end
  it "Service Order Sender Phone can not be more than 15 characters" do 
    service_order = sos(:sender_phone_long).should_not be_valid
  end

  it "Service Order Sender Phone invalid" do 
    service_order = sos(:sender_phone_invalid).should_not be_valid
  end



  #TOTAL_AMOUNT

  it "service order should have total amount" do
    service_order = So.new(:total_amount => 10)
    assert service_order.save(validate: false), "It was not possible to insert a total amount"
  end

  it "Service Order Total Amount not should be nil" do 
    service_order = sos(:total_amount_nil).should_not be_valid 
  end

  #RELACIONAMENTOS

  it "Material Class ID not should be nil" do 
    service_order = sos(:material_class_id_nil).should_not be_valid 
  end

  it "Military Organization ID not should be nil" do 
    service_order = sos(:military_organization_id_nil).should_not be_valid 
  end
end
