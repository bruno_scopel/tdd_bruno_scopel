require 'spec_helper'
require 'rails_helper'

RSpec.describe MaterialClass, :type => :model do

  fixtures :material_classes

  it "material class should be saved" do
      material_class = MaterialClass.new
      assert material_class.save(validate: false), "It was not possible to save a material class"
  end

  #NAME
  
  it "material class should have name" do
      material_class = MaterialClass.new(:name => "material")
      assert material_class.save(validate: false), "It was not possible to insert a name"
  end

  it "Material Class Name not should be nil" do 
    material = material_classes(:name_nil).should_not be_valid 
  end

  it "Material Class Name can not be less than 2 characters" do 
    material = material_classes(:short_name).should_not be_valid 
  end

  it "Material Class Name can not be more than 30 characters" do 
    material = material_classes(:long_name).should_not be_valid
  end

  #ACRONYM
  
  it "material class should have acronym" do
      material_class = MaterialClass.new(:acronym => "CLII")
      assert material_class.save(validate: false), "It was not possible to insert a acronym"
  end

  it "Material Class Acronym not should be nil" do 
    material = material_classes(:acronym_nil).should_not be_valid
  end
  
  it "Material Class Acronym can not be less than 2 characters" do 
    material = material_classes(:short_acronym).should_not be_valid 
  end
  
  it "Material Class Acronym can not be more than 15 characters" do 
    material = material_classes(:long_acronym).should_not be_valid
  end

end