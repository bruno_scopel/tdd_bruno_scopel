require 'spec_helper'
require 'rails_helper'

RSpec.describe MilitaryOrganization, :type => :model do
  fixtures :military_organizations

  it "military organization should be saved" do
      military_organization = MilitaryOrganization.new
      assert military_organization.save(validate: false), "It was not possible to save a material class"
  end

  

  #NAME
  
  it "military organization should have name" do
      military_organization = MilitaryOrganization.new(:name => "Batalhao")
      assert military_organization.save(validate: false), "It was not possible to insert a name"
  end

  it "Military Organization Name not should be nil" do 
    miliary_org = military_organizations(:name_nil).should_not be_valid 
  end
  
  it "Military Organization Name can not be less than 2 characters" do 
    miliary_org = military_organizations(:short_name).should_not be_valid 
  end

  it "Military Organization Name can not be more than 30 characters" do 
    miliary_org = military_organizations(:long_name).should_not be_valid
  end

  #ACRONYM
  
  it "military organization should have acronym" do
      military_organization = MilitaryOrganization.new(:acronym => "Btl")
      assert military_organization.save(validate: false), "It was not possible to insert a acronym"
  end

  it "Military Organization Acronym not should be nil" do 
    miliary_org = military_organizations(:acronym_nil).should_not be_valid
  end
 
  it "Military Organization Acronym can not be less than 2 characters" do 
    miliary_org = military_organizations(:short_acronym).should_not be_valid 
  end
  
  it "Military Organization Acronym can not be more than 20 characters" do 
    miliary_org = military_organizations(:long_acronym).should_not be_valid
  end
end
