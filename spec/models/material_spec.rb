require 'spec_helper'
require 'rails_helper'

RSpec.describe Material, :type => :model do

  fixtures :materials


  it "material should be saved" do
    material = Material.new
    assert material.save(validate: false), "It was not possible to save a material"
  end

  #Name

  it "material should have name" do
      material = Material.new(:name => "parafuso")
      assert material.save(validate: false) , "It was not possible to insert a name"
  end

  it "Material Name not should be nil" do 
    material = materials(:name_nil).should_not be_valid 
  end

   it "Material Name can not be less than 2 characters" do 
    material = materials(:short_name).should_not be_valid 
  end

  it "Material Name can not be more than 50 characters" do 
    material = materials(:long_name).should_not be_valid
  end

  #AMOUNT

  it "material should have amount" do
      material = Material.new(:amount => 2.5)
      assert material.save(validate: false) , "It was not possible to insert a amount"
  end

  it "Material Amount not should be nil" do 
    material = materials(:amount_nil).should_not be_valid
  end

  #VALUE

  it "material should have value" do
      material = Material.new(:value => 2.5)
      assert material.save(validate: false) , "It was not possible to insert a value"
  end

  it "Material Value not should be nil" do 
    material = materials(:value_nil).should_not be_valid 
  end

  #UNIT

  it "material should have value" do
      material = Material.new(:unit => "unidade")
      assert material.save(validate: false) , "It was not possible to insert a unit"
  end

  it "Material Unit not should be nil" do 
    material = materials(:unit_nil).should_not be_valid
  end
 
  it "Material Unit can not be less than 2 characters" do 
    material = materials(:short_unit).should_not be_valid 
  end
  it "Material Unit can not be more than 20 characters" do 
   material = materials(:long_unit).should_not be_valid
  end

  #Relacionamento 

  it "Sub Service Order ID not should be nil" do 
    material = materials(:sub_service_order_id_nil).should_not be_valid
  end
end
