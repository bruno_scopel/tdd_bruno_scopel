class SectionMilitaryOrganizationsController < ApplicationController
	def index
		@section_military_organizations = SectionMilitaryOrganization.all
	end

	def new
		@section_military_organization = SectionMilitaryOrganization.new
	end
	
	def create
		@section_military_organization = SectionMilitaryOrganization.new(section_military_organization_params)
		if @section_military_organization.save then
			redirect_to section_military_organizations_path
		else
			render :new
		end
	end

	def destroy
		begin
			@section_military_organization = SectionMilitaryOrganization.find(params[:id])
			if @section_military_organization.sub_service_orders == nil or @section_military_organization.sub_service_orders.length == 0 then
				@section_military_organization.destroy
			end
				
			redirect_to section_military_organizations_path

		rescue ActiveRecord::RecordNotFound
			redirect_to section_military_organizations_path
		end		
	end

	def edit
		begin
			@section_military_organization = SectionMilitaryOrganization.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			redirect_to section_military_organizations_path
		end
	end

	def update
  		begin
  			@section_military_organization = SectionMilitaryOrganization.find(params[:id])
  			if @section_military_organization.update(section_military_organization_params) then 
  				redirect_to section_military_organizations_path
  			else
  				render :edit
  			end
  		rescue ActiveRecord::RecordNotFound
  			redirect_to section_military_organizations_path
  		end
	end
	# Never trust parameters from the scary internet, only allow the white list through.
    def section_military_organization_params
      params.require(:section_military_organization).permit(:name, :acronym)
    end
end
