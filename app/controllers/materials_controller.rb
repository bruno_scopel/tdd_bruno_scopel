class MaterialsController < ApplicationController

	def new
		begin
			@so = So.find(params[:so_id])
			@sub_service_order = SubServiceOrder.find(params[:sub_service_order_id])
		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				redirect_to sos_path
			end
		end
		@material = Material.new
	end

	def create
		begin
			@so = So.find(params[:so_id])
			@sub_service_order = SubServiceOrder.find(params[:sub_service_order_id])
			@material = Material.new(material_params)

			if @material.save then
				redirect_to so_sub_service_order_path(@material.sub_service_order.so, @material.sub_service_order)
			else
				render :new, {so_id: @so, sub_service_order_id: @sub_service_order}
			end
		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				redirect_to sos_path
			end
		end
	end

	def destroy
		begin
			@so = So.find(params[:so_id])
			@sub_service_order = SubServiceOrder.find(params[:sub_service_order_id])
			@material = Material.find(params[:id])

			@material.destroy

			redirect_to so_sub_service_order_path(@so, @sub_service_order)
			
		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				if e.message.include?("So") 
					redirect_to sos_path
				else
					redirect_to so_sub_service_order_path(@so, @sub_service_order)
				end
			end
		end
	end

	# Never trust parameters from the scary internet, only allow the white list through.
    def material_params
      params.require(:material).permit(:name, :amount, :value, :unit, :sub_service_order_id)
    end
end
