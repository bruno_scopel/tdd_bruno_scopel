class MilitaryOrganizationsController < ApplicationController
	def index
		@military_organizations = MilitaryOrganization.all
	end

	def new
		@military_organization = MilitaryOrganization.new
	end

	def create
		@military_organization = MilitaryOrganization.new(military_organization_params)
		if @military_organization.save then
			redirect_to military_organizations_path
		else
			render :new
		end
	end

	def destroy
		begin
			@military_organization = MilitaryOrganization.find(params[:id])

			if @military_organization.sos == nil or @military_organization.sos.length == 0 then
				@military_organization.destroy
			end

			redirect_to military_organizations_path
			
		rescue ActiveRecord::RecordNotFound
			redirect_to military_organizations_path
		end	
	end

	def edit
		begin
			@military_organization = MilitaryOrganization.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			redirect_to military_organizations_path
		end
	end

	def update
  		begin
  			@military_organization = MilitaryOrganization.find(params[:id])
  			if @military_organization.update(military_organization_params) then 
  				redirect_to military_organizations_path
  			else
  				render :edit
  			end
  		rescue ActiveRecord::RecordNotFound
  			redirect_to military_organizations_path
  		end
	end

	def military_organization_params
      params.require(:military_organization).permit(:name, :acronym)
    end
end
