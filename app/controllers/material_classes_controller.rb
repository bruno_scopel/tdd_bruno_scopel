class MaterialClassesController < ApplicationController
	def index
		@material_classes = MaterialClass.all
	end

	def new
		@material_class = MaterialClass.new
	end

	def create
		@material_class = MaterialClass.new(material_class_params)
		if @material_class.save then
			redirect_to material_classes_path
		else
			render :new
		end
	end

	def destroy
		begin
			@material_class = MaterialClass.find(params[:id])

			if @material_class.sos == nil or @material_class.sos.length == 0 then
				@material_class.destroy
			end

			redirect_to material_classes_path

		rescue ActiveRecord::RecordNotFound
			redirect_to material_classes_path
		end	
	end

	def edit
		begin
			@material_class = MaterialClass.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			redirect_to material_classes_path
		end
	end

	def update
		begin
  			@material_class = MaterialClass.find(params[:id])
  			if @material_class.update(material_class_params) then 
  				redirect_to material_classes_path
  			else
  				render :edit
  			end
  		rescue ActiveRecord::RecordNotFound
  			redirect_to material_classes_path
  		end
	end

	# Never trust parameters from the scary internet, only allow the white list through.
    def material_class_params
      params.require(:material_class).permit(:name, :acronym)
    end
end
