class SosController < ApplicationController
	def index
		@sos = So.all
	end

	def new
		@so = So.new
	end

	def create
		@so = So.new(so_params)
		if @so.save then
			redirect_to sos_path
		else
			render :new
		end
	end

	def show
		begin
			@so = So.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			redirect_to sos_path
		end
	end

	def destroy
		begin
			@so = So.find(params[:id])
			if @so.sub_service_orders == nil or @so.sub_service_orders.length == 0 then
				@so.destroy
			end	
			redirect_to sos_path
		rescue ActiveRecord::RecordNotFound
			redirect_to sos_path
		end		
	end

	def edit
		begin
			@so = So.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			redirect_to sos_path
		end
	end

	def update
		begin
  			@so = So.find(params[:id])
  			if @so.update(so_params) then 
  				redirect_to sos_path
  			else
  				render :edit
  			end
  		rescue ActiveRecord::RecordNotFound
  			redirect_to sos_path
  		end
	end

	private

    # Never trust parameters from the scary internet, only allow the white list through.
    def so_params
      params.require(:so).permit(:material_description, :guide_number, :sender_name, :sender_phone, :total_amount, :material_class_id, :military_organization_id)
    end

end
