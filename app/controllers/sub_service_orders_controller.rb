class SubServiceOrdersController < ApplicationController
	
	def new
		begin
			@so = So.find(params[:so_id])
		rescue ActiveRecord::RecordNotFound
			redirect_to sos_path
		end

		@sub_service_order = SubServiceOrder.new
	end

	def create
		begin
			@so = So.find(params[:so_id])

			@sub_service_order = SubServiceOrder.new(sub_service_order_params)

			if @sub_service_order.save then
				redirect_to so_path(@so)
			else
				@so = So.find(params[:so_id])
				render :new, {so_id: @so.id}
			end
		rescue ActiveRecord::RecordNotFound
			redirect_to sos_path
		end
	end

	def show
		begin
			@so = So.find(params[:so_id])
			@sub_service_order = SubServiceOrder.find(params[:id])
		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				redirect_to sos_path
			end
		end	
	end

	def destroy
		begin
			@so = So.find(params[:so_id])
			@sub_service_order = SubServiceOrder.find(params[:id])

			if @sub_service_order.destroy
				redirect_to so_path(@so)	
			end

		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				redirect_to sos_path
			end
		end	
	end

	def edit
		begin
			@so = So.find(params[:so_id])
			@sub_service_order = SubServiceOrder.find(params[:id])

		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				redirect_to sos_path
			end
		end	
	end

	def update
		begin
			@so = So.find(params[:so_id])
  			@sub_service_order = SubServiceOrder.find(params[:id])
  			if @sub_service_order.update(sub_service_order_params) then 
  				redirect_to so_path(@sub_service_order.so_id)
  			else
  				render :edit
  			end
  		rescue ActiveRecord::RecordNotFound => e
			if e.message.include?("SubServiceOrder") 
				redirect_to so_path(@so)
			else
				redirect_to sos_path
			end
		end	
	end

	private

    # Never trust parameters from the scary internet, only allow the white list through.
    def sub_service_order_params
      params.require(:sub_service_order).permit(:description, :amount, :service_description, :technical, :so_id, :section_military_organization_id)
    end
end
