class MilitaryOrganization < ActiveRecord::Base
	validates_presence_of :name
	validates_presence_of :acronym

	validates_length_of :name, :minimum => 2
	validates_length_of :name, :maximum => 60
	validates_length_of :acronym, :minimum => 2
	validates_length_of :acronym, :maximum => 15

	#RELACIONAMENTOS
	has_many :sos
end
