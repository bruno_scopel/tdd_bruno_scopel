class SubServiceOrder < ActiveRecord::Base
	  validates_presence_of :description
    validates_presence_of :amount
   	validates_presence_of :service_description
  	validates_presence_of :technical
  	
  	validates_length_of :description, :minimum => 2
  	validates_length_of :description, :maximum => 500
  	validates_length_of :service_description, :minimum => 2
  	validates_length_of :service_description, :maximum => 500
  	validates_length_of :technical, :minimum => 2
  	validates_length_of :technical, :maximum => 30
  
  	validates_presence_of :so_id
  	validates_presence_of :section_military_organization_id
  
  	#relacionamentos
  	
  	has_many :materials
  	belongs_to :so
  	belongs_to :section_military_organization
end
