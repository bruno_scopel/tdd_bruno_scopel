class So < ActiveRecord::Base
	  validates_presence_of :material_description
	 
	  validates_presence_of :sender_name
	  validates_presence_of :sender_phone
	  validates_presence_of :total_amount
    validates_presence_of :guide_number
	  validates_length_of :material_description, :minimum => 2
	  validates_length_of :material_description, :maximum => 100
    validates_length_of :sender_name, :minimum => 2
  	validates_length_of :sender_name, :maximum => 20
  	validates_length_of :sender_phone, :minimum => 13
  	validates_length_of :sender_phone, :maximum => 15
  	validates :guide_number,length: { maximum: 10 }       
    validates :guide_number,length: { minimum: 2 } 
  
  	validates_format_of :sender_phone, :with => /\((\d{2})\)+(\d{4})-(\d{4})/
  
  	#validacoes relacionamento
  	validates_presence_of :material_class_id
  	validates_presence_of :military_organization_id
  
  
  	#relacionamentos
  	belongs_to :material_class  
  	belongs_to :military_organization
  	has_many :sub_service_orders
end
