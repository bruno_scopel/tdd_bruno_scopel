class Material < ActiveRecord::Base
	  validates_presence_of :name
  	validates_presence_of :amount
  	validates_presence_of :value
  	validates_presence_of :unit
  	validates_length_of :name, :minimum => 2
  	validates_length_of :name, :maximum => 50
  	validates_length_of :unit, :minimum => 2
  	validates_length_of :unit, :maximum => 20
  

    validates_presence_of :sub_service_order_id
  
  	#relacionamentos
  	belongs_to :sub_service_order
end
