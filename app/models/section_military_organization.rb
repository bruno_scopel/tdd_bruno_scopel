class SectionMilitaryOrganization < ActiveRecord::Base
	validates_presence_of :name
  	validates_presence_of :acronym
  	validates_length_of :name, :minimum => 2
  	validates_length_of :name, :maximum => 30
  	validates_length_of :acronym, :minimum => 2
  	validates_length_of :acronym, :maximum => 20
  
  	#relacionamentos
  	has_many :sub_service_orders
end
