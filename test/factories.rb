FactoryGirl.define do
  factory :material_class do
    name "FACTORY"
    acronym  "FACTORY"
  end

  factory :material_class_without_relationship, class: MaterialClass do
    name "FACTORY 2"
    acronym "FACTORY 2"
  end

  factory :military_organization do
    name "Name_fact"
    acronym  "Acronym_fact_mo"
  end

  factory :section_military_organization do
    name "Sect_Name_fact"
    acronym  "Acronym_fact_smo"
  end

  factory :so do
    material_description "Blindado_fact"
  	guide_number 11
  	sender_name  "Ten Ciclano"
  	sender_phone "(67)1234-1123"
  	total_amount 1
  	material_class
    military_organization
  end

  factory :sub_service_order do
    description "description_fact"
  	amount 2
  	service_description "service_description_fact"
  	technical "technical_fact"
    section_military_organization
    so
  end

  factory :material do
  	name "MyName_fact"
  	amount 1.5
  	value 2.5
  	unit "MyUnit_fact"
    sub_service_order
  end
end