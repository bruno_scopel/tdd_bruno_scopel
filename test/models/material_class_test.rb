require 'test_helper'

class MaterialClassTest < ActiveSupport::TestCase

	def setup
		create(:material_class) 
	end

  	#test "Factory esta sendo trazida para o banco de testes" do
  	#	material_classes = MaterialClass.all

  	#	material_classes.each do |material_class|
  	#		puts material_class.name
  	#	end
  	#end


	test "material class should be saved" do
    	material_class = MaterialClass.new

    	assert material_class.save(validate: false), "It was not possible to save a material class"
  	end

  	#NAME
 	
 	test "material class should have name" do
  		material_class = MaterialClass.new(:name => "material")

  		assert material_class.save(validate: false), "It was not possible to insert a name"
  	end

	test "material name must not be null" do
		my_material = build(:material_class, :name => nil)

	    assert !my_material.save, "Saved with name NIL"
	end

	test "material name must have at least 2 characters" do
	    my_material = build(:material_class, :name => "a")

	    assert !my_material.save, "Saved with name shorter than 2 characters"
	end

	test "material name must have less than 30 characters" do
	    my_material = build(:material_class, :name => "qwertyuiopasdfghjklçzxcvbnmqwer")

	    assert !my_material.save, "Saved with name 31-character-long"
	end

	#ACRONYM
 	
 	test "material class should have acronym" do
  		material_class = MaterialClass.new(:acronym => "CLII")

  		assert material_class.save(validate: false), "It was not possible to insert a acronym"
  	end
	  
	test "material acronym must not be null" do
	    my_material = build(:material_class, :acronym => nil)

	    assert !my_material.save, "Saved with acronym NIL"
	end
  
	test "material acronym must have at least 2 characters" do
	    my_material = build(:material_class, :acronym => "a")

	    assert !my_material.save, "Saved with acronym shorter than 2 characters"
	end

	test "material acronym must have less than 15 characters" do
	    my_material = build(:material_class, :acronym => "wertyuiopasdfghk")

	    assert !my_material.save, "Saved with acronym 15-character-long"
	end
end
