require 'test_helper'

class SoTest < ActiveSupport::TestCase
   #fixtures :sos
   #fixtures :material_classes

    def setup
      create(:so) 
    end

    test "service order should be saved" do
      service_order = So.new

      assert service_order.save(validate: false), "It was not possible to save a service order"
    end

    #MATERIAL_DESCRIPTION
  
    test "service order should have material description" do
      service_order = So.new(:material_description => "My material")

      assert service_order.save(validate: false), "It was not possible to insert a material_description"
    end

    test "service order must have material description" do
      service_order = build(:so, :material_description => nil)

      assert !service_order.save, "It was possible to save a service order without a material_description"
    end

    test "service order must have material description with at least 2 characters" do
      service_order = build(:so, :material_description => "a")

      assert !service_order.save, "It was possible to save a service order with material description shorter than 2 characters"
    end

    test "service order must have material description with less than 100 characters" do
      service_order = build(:so, :material_description => "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeee1")

      assert !service_order.save, "It was possible to save a service order with material description with longer than 100 characters"
    end


    #test "service order must have material description" do
    #  service_order = sos(:material_description_nil)

    #  assert !service_order.save, "It was possible to save a service order without a material_description"
    #end

    #test "service order must have material description with at least 2 characters" do
    #  service_order = sos(:material_description_short)

    #  assert !service_order.save, "It was possible to save a service order with material description shorter than 2 characters"
    #end

    #test "service order must have material description with less than 100 characters" do
    #  service_order = sos(:material_description_long)

    #  assert !service_order.save, "It was possible to save a service order with material description with longer than 100 characters"
    #end


    #GUIDE_NUMBER

    test "service order should have guide number" do
      service_order = So.new(:guide_number => "2014001")

      assert service_order.save(validate: false), "It was not possible to insert a guide_number"
    end

    test "service order must have guide number" do
      service_order = build(:so, :guide_number => nil)

      assert !service_order.save, "It was not possible to save service order without a guide_number"
    end

    test "SO must have guide number with at least 2 characters" do
      
      service_order = build(:so, :guide_number => 1)
        
      assert !service_order.save, "It was possible to save a service order with guide number shorter than 2 characters"
    end

    test "SO must have guide number with less than 10 characters" do
      
      service_order = build(:so, :guide_number => 12345123451)

      assert !service_order.save, "It was possible to save a service order with guide number with longer than 10 characters"
    end

    

    #test "service order must have guide number" do
    #  service_order = sos(:guide_number_nil)
    #  assert !service_order.save, "It was not possible to save service order without a guide_number"
    #end

    #test "service order must have guide number with at least 2 characters" do
    #  service_order = sos(:guide_number_short)
    #  assert !service_order.save, "It was possible to save a service order with guide number shorter than 2 characters"
    #end

    #test "service order must have guide number with less than 10 characters" do
    #  service_order = sos(:guide_number_long)
    #  assert !service_order.save, "It was possible to save a service order with guide number with longer than 10 characters"
    #end


    #SENDER_NAME

    test "service order should have sender name" do
      service_order = So.new(:sender_name => "Cb Fulano")

      assert service_order.save(validate: false), "It was not possible to insert a sender name"
    end

    test "service order must have sender name" do
      service_order = build(:so, :sender_name => nil)

      assert !service_order.save, "It was not possible to save service order without a sender name"
    end

    test "service order must have sender name with at least 2 characters" do
      service_order = build(:so, :sender_name => "a")

      assert !service_order.save, "It was possible to save a service order with sender name shorter than 2 characters"
    end

    test "service order must have sender name with less than 20 characters" do
      service_order = build(:so, :sender_name => "aaaaaaaaaabbbbbbbbbb1")

      assert !service_order.save, "It was possible to save a service order with sender name with longer than 20 characters"
    end

    #test "service order must have sender name" do
    #  service_order = sos(:sender_name_nil)

    #  assert !service_order.save, "It was not possible to save service order without a sender name"
    #end

    #test "service order must have sender name with at least 2 characters" do
    #  service_order = sos(:sender_name_short)

    #  assert !service_order.save, "It was possible to save a service order with sender name shorter than 2 characters"
    #end

    #test "service order must have sender name with less than 20 characters" do
    #  service_order = sos(:sender_name_long)

    #  assert !service_order.save, "It was possible to save a service order with sender name with longer than 20 characters"
    #end

    #SENDER_PHONE

    test "service order should have sender phone" do
      service_order = So.new(:sender_phone => "(xx)xxxx-xxxx")

      assert service_order.save(validate: false), "It was not possible to insert a sender phone"
    end

    test "service order must have sender phone" do
      service_order = build(:so, :sender_phone => nil)

      assert !service_order.save, "It was not possible to save service order without a sender phone"
    end

    test "service order must have sender phone with at least 13 characters" do
      service_order = build(:so, :sender_phone => "(67)12345678")

      assert !service_order.save, "It was possible to save a service order with sender phone shorter than 13 characters"
    end

    test "service order must have sender phone with less than 15 characters" do
      service_order = build(:so, :sender_phone => "(67)1234-1123333")

      assert !service_order.save, "It was possible to save a service order with sender phone with longer than 15 characters"
    end

    test "service order sender phone invalid" do
      service_order = build(:so, :sender_phone => "aaaa3321-2777")

      assert !service_order.save, "Saved with sender phone invalid"
    end

    #test "service order must have sender phone" do
    # service_order = sos(:sender_phone_nil)

    #  assert !service_order.save, "It was not possible to save service order without a sender phone"
    #end

    #test "service order must have sender phone with at least 13 characters" do
    #  service_order = sos(:sender_phone_short)

    #  assert !service_order.save, "It was possible to save a service order with sender phone shorter than 13 characters"
    #end

    #test "service order must have sender phone with less than 15 characters" do
    #  service_order = sos(:sender_phone_long)

    #  assert !service_order.save, "It was possible to save a service order with sender phone with longer than 15 characters"
    #end

    #test "service order sender phone invalid" do
    #  service_order = sos(:sender_phone_invalid)

    #  assert !service_order.valid?, "Saved with sender phone invalid"
    #end

    #TOTAL_AMOUNT

    test "service order should have total amount" do
      service_order = So.new(:total_amount => 10)

      assert service_order.save(validate: false), "It was not possible to insert a total amount"
    end

    test "service order must have total amount" do
      service_order = build(:so, :total_amount => nil)
      assert !service_order.save, "It was not possible to save service order without a total amount"
    end

    #test "service order must have total amount" do
    #  service_order = sos(:total_amount_nil)

    #  assert !service_order.save, "It was not possible to save service order without a total amount"
    #end

    #RELACIONAMENTOS

    #test "service order material class id must not be null" do
    #  service_order = sos(:material_class_id_nil)
    #  assert !service_order.save, "Saved with material class id NIL"
    #end
  
    #test "service order military organization id must not be null" do   
    # service_order = sos(:military_organization_id_nil) 
    #  assert !service_order.save, "Saved with military organization id NIL"
    #end

    test "service order material class id must not be null" do
      service_order = build(:so, :material_class_id => nil)
      assert !service_order.save, "Saved with material class id NIL"
    end
  
    test "service order military organization id must not be null" do   
      service_order = build(:so, :military_organization_id => nil)
      assert !service_order.save, "Saved with military organization id NIL"
    end
end
