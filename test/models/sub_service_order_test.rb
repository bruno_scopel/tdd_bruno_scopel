require 'test_helper'

class SubServiceOrderTest < ActiveSupport::TestCase
  #fixtures :sub_service_orders
  def setup
    create(:sub_service_order) 
  end

	test "sub service order should be saved" do
    	sub_service_order = SubServiceOrder.new

    	assert sub_service_order.save(validate: false), "It was not possible to save a sub service order"
  	end

  #DESCRIPTION
 	
 	test "sub service order should have description" do
  		sub_service_order = SubServiceOrder.new(:description => "material x")
  		assert sub_service_order.save(validate: false), "It was not possible to insert a description"
  	end

  test "sub service order must have material description" do
  		sub_service_order = build(:sub_service_order, :description => nil)
  		assert !sub_service_order.save, "It was possible to save a sub service order without a description"
 	end

 	test "sub service order description must have at least 2 characters" do
     	my_sub_service_order = build(:sub_service_order, :description => "a")
     	assert !my_sub_service_order.valid?, "Saved with description shorter than 2 characters"
  end

  test "sub service order description must have less than 500 characters" do
     	my_sub_service_order = build(:sub_service_order, :description => "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeee1")
     	assert !my_sub_service_order.valid?, "Saved with name 501-character-long"
  end

  #  test "sub service order must have material description" do
  #    sub_service_order = sub_service_orders(:description_nil)
  #    assert !sub_service_order.save, "It was possible to save a sub service order without a description"
  #  end

  #  test "sub service order description must have at least 2 characters" do
  #      my_sub_service_order = sub_service_orders(:description_short)
  #      assert !my_sub_service_order.valid?, "Saved with description shorter than 2 characters"
  #  end

  #  test "sub service order description must have less than 500 characters" do
  #      my_sub_service_order = sub_service_orders(:description_long)
  #      assert !my_sub_service_order.valid?, "Saved with name 501-character-long"
  #  end


 	#AMOUNT

 	test "sub service order should have amount" do
  		sub_service_order = SubServiceOrder.new(:amount => 1)
  		assert sub_service_order.save(validate: false), "It was not possible to insert a amount"
  	end

 	#test "sub service order amount must not be null" do
  #  	my_sub_service_order = sub_service_orders(:amount_nil)
  #  	assert !my_sub_service_order.save, "Saved with amount NIL"
  #end

  test "sub service order amount must not be null" do
      my_sub_service_order = build(:sub_service_order, :amount => nil)
      assert !my_sub_service_order.save, "Saved with amount NIL"
  end

 	#SERVICE_DESCRIPTION

 	test "sub service order should have service description" do
  		sub_service_order = SubServiceOrder.new(:service_description => "material x")
  		assert sub_service_order.save(validate: false), "It was not possible to insert a service description"
  	end

 	test "sub service order service description must not be null" do
   	my_sub_service_order = build(:sub_service_order, :service_description => nil)
   	assert !my_sub_service_order.save, "Saved with service description NIL"
  end

  test "sub service order service description must have at least 2 characters" do
   	my_sub_service_order = build(:sub_service_order, :service_description => "a")
   	assert !my_sub_service_order.valid?, "Saved with description shorter than 2 characters"
  end

  test "sub service order service description must have less than 500 characters" do
   	my_sub_service_order = build(:sub_service_order, :service_description => "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeaaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeee1")
   	assert !my_sub_service_order.valid?, "Saved with name 501-character-long"
  end

  #test "sub service order service description must have at least 2 characters" do
  #  my_sub_service_order = sub_service_orders(:service_description_short)
  #  assert !my_sub_service_order.valid?, "Saved with description shorter than 2 characters"
  #end

  #test "sub service order service description must have less than 500 characters" do
  #  my_sub_service_order = sub_service_orders(:service_description_long)
  #  assert !my_sub_service_order.valid?, "Saved with name 501-character-long"
  #end

 	#TECHNICAL

 	test "sub service order should have technical" do
  		sub_service_order = SubServiceOrder.new(:description => "sgt ciclano")
  		assert sub_service_order.save(validate: false), "It was not possible to insert a technical"
  	end
  
 	test "sub service order technical must not be null" do
    	my_sub_service_order = build(:sub_service_order, :technical => nil)
    	assert !my_sub_service_order.save, "Saved with technical NIL"
  	end
  
  	test "sub service order technical must have at least 2 characters" do
      	my_sub_service_order = build(:sub_service_order, :technical => "a")
      	assert !my_sub_service_order.valid?, "Saved with description shorter than 2 characters"
  	end

  	test "sub service order technical must have less than 30 characters" do
      	my_sub_service_order = build(:sub_service_order, :technical => "aaaaaaaaaabbbbbbbbbbcccccccccc1")
      	assert !my_sub_service_order.valid?, "Saved with name 31-character-long"
  	end

    #test "sub service order technical must have at least 2 characters" do
    #    my_sub_service_order = sub_service_orders(:technical_short)
    #    assert !my_sub_service_order.valid?, "Saved with description shorter than 2 characters"
    #end

    #test "sub service order technical must have less than 30 characters" do
    #    my_sub_service_order = sub_service_orders(:technical_long)
    #    assert !my_sub_service_order.valid?, "Saved with name 31-character-long"
    #end

	#RELACIONAMENTOS  	
  
  	test "sub service order service order id must not be null" do   
    	my_sub_service_order = build(:sub_service_order, :so_id => nil)
    	assert !my_sub_service_order.save, "Saved with service order id NIL"
  	end
  
  	test "sub service order section military organization id must not be null" do   
     	my_sub_service_order = build(:sub_service_order, :section_military_organization_id => nil)
    	assert !my_sub_service_order.save, "Saved with section military organization id NIL"
    end
end
