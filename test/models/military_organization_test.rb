require 'test_helper'

class MilitaryOrganizationTest < ActiveSupport::TestCase
  
  	def setup
      create(:military_organization)
    end

    test "military organization should be saved" do
    	military_organization = MilitaryOrganization.new

    	assert military_organization.save(validate: false), "It was not possible to save a material class"
  	end

  	#NAME
 	
 	test "military organization should have name" do
  		military_organization = MilitaryOrganization.new(:name => "Batalhao")

  		assert military_organization.save(validate: false), "It was not possible to insert a name"
  	end

	test "military organization name must not be null" do
		military_organization = build(:military_organization, :name => nil)

	    assert !military_organization.save, "Saved with name NIL"
	end

	test "military organization name must have at least 2 characters" do
	    military_organization = build(:military_organization, :name => "a")

	    assert !military_organization.valid?, "Saved with name shorter than 2 characters"
	end

	test "military organization name must have less than 60 characters" do
	    military_organization = build(:military_organization, :name => "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffff1")

	    assert !military_organization.valid?, "Saved with name 61-character-long"
	end

	#ACRONYM
 	
 	test "military organization should have acronym" do
  		military_organization = MilitaryOrganization.new(:acronym => "Btl")

  		assert military_organization.save(validate: false), "It was not possible to insert a acronym"
  	end
	  
	test "military organization acronym must not be null" do
	    military_organization = build(:military_organization, :acronym => nil)

	    assert !military_organization.save, "Saved with acronym NIL"
	end
  
	test "military organization acronym must have at least 2 characters" do
	    military_organization = build(:military_organization, :acronym => "a")

	    assert !military_organization.valid?, "Saved with acronym shorter than 2 characters"
	end

	test "military organization acronym must have less than 15 characters" do
	    military_organization = build(:military_organization, :acronym => "qwertyuiopasdfgh")

	    assert !military_organization.valid?, "Saved with acronym 15-character-long"
	end
end
