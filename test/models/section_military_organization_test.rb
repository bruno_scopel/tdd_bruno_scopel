require 'test_helper'

class SectionMilitaryOrganizationTest < ActiveSupport::TestCase
  
  def setup
    create(:section_military_organization) 
  end

  test "section military organization should be saved" do
    	section = SectionMilitaryOrganization.new

    	assert section.save(validate: false), "It was not possible to save a section military organization"
  end

  #NAME
 	
  test "section military organization should have name" do
  		section = SectionMilitaryOrganization.new(:name => "Informatica")
  
  		assert section.save(validate: false), "It was not possible to insert a name"
  end

  #test "section name must not be null" do
  #  my_section = section_military_organizations(:name_nil)
  #  assert !my_section.save, "Saved with name NIL"
  #end

  #test "section name must have at least 2 characters" do
  #    my_section = section_military_organizations(:short_name)
  #    assert !my_section.valid?, "Saved with name shorter than 2 characters"
  #end

  #test "section name must have less than 30 characters" do
  #    my_section = section_military_organizations(:long_name)
  #    assert !my_section.valid?, "Saved with name 31-character-long"
  #end

  test "section name must not be null" do
    my_section = build(:section_military_organization, :name => nil)
    assert !my_section.save, "Saved with name NIL"
  end

  test "section name must have at least 2 characters" do
      my_section = build(:section_military_organization, :name => "a")
      assert !my_section.valid?, "Saved with name shorter than 2 characters"
  end

  test "section name must have less than 30 characters" do
      my_section = build(:section_military_organization, :name => "aaaaaaaaaabbbbbbbbbbcccccccccc1")
      assert !my_section.valid?, "Saved with name 31-character-long"
  end

  #ACRONYM
 	
  test "section military organization should have acronym" do
  		section = SectionMilitaryOrganization.new(:acronym => "Sinfo")

  		assert section.save(validate: false), "It was not possible to insert a acronym"
  end

  #test "section acronym must not be null" do
  #  my_section = section_military_organizations(:acronym_nil)
  #  assert !my_section.save, "Saved with acronym NIL"
  #end
  
  #test "section acronym must have at least 2 characters" do
  #    my_section = section_military_organizations(:short_acronym)
  #    assert !my_section.valid?, "Saved with acronym shorter than 2 characters"
  #end

  #test "section acronym must have less than 20 characters" do
  #    my_section = section_military_organizations(:long_acronym)
  #    assert !my_section.valid?, "Saved with acronym 20-character-long"
  #end

  test "section acronym must not be null" do
    my_section = build(:section_military_organization, :acronym => nil)
    assert !my_section.save, "Saved with acronym NIL"
  end
  
  test "section acronym must have at least 2 characters" do
      my_section = build(:section_military_organization, :acronym => "a")
      assert !my_section.valid?, "Saved with acronym shorter than 2 characters"
  end

  test "section acronym must have less than 20 characters" do
      my_section = build(:section_military_organization, :acronym => "aaaaaaaaaabbbbbbbbbb1")
      assert !my_section.valid?, "Saved with acronym 20-character-long"
  end
end
