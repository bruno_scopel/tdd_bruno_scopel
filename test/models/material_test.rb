require 'test_helper'

class MaterialTest < ActiveSupport::TestCase

    def setup
      create(:material)
    end

  	test "material should be saved" do
    	material = Material.new

    	assert material.save(validate: false), "It was not possible to save a material"
  	end

  	#NAME
 	
 	  test "material should have name" do
  		material = Material.new(:name => "parafuso")

  		assert material.save(validate: false), "It was not possible to insert a name"
  	end

	  #test "material name must not be null" do
    #	my_material = materials(:name_nil)
    #	assert !my_material.save, "Saved with name NIL"
  	#end

    test "material name must not be null" do
      my_material = build(:material, :name => nil)
      assert !my_material.save, "Saved with name NIL"
    end

  	#test "material name must have at least 2 characters" do
    #  	my_material = materials(:short_name)
    #  	assert !my_material.valid?, "Saved with name shorter than 2 characters"
  	#end

    test "material name must have at least 2 characters" do
        my_material = build(:material, :name => "a")
        assert !my_material.save, "Saved with name shorter than 2 characters"
    end

  	#test "material name must have less than 50 characters" do
    #  	my_material = materials(:long_name)
    #  	assert !my_material.valid?, "Saved with name 51-character-long"
  	#end

    test "material name must have less than 50 characters" do
        my_material = build(:material, :name => "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeee1")
        assert !my_material.save, "Saved with name 51-character-long"
    end
  	

  	#AMOUNT
 	
 	  test "material should have amount" do
  		material = Material.new(:amount => 2.5)
  		assert material.save(validate: false), "It was not possible to insert a amount"
  	end

  	#test "material amount must not be null" do
    #	my_material = materials(:amount_nil)
    #	assert !my_material.save, "Saved with amount NIL"
  	#end

    test "material amount must not be null" do
      my_material = build(:material, :amount => nil)
      assert !my_material.save, "Saved with amount NIL"
    end

  	#VALUE
 	
 	  test "material should have value" do
  		material = Material.new(:value => 1.5)
  		assert material.save(validate: false), "It was not possible to insert a value"
  	end

  	#test "material value must not be null" do
    #	my_material = #
    # assert !my_mate#rial.save, "Saved with value NIL"
  	#end

    test "material value must not be null" do
      my_material = build(:material, :value => nil)
      assert !my_material.save, "Saved with value NIL"
    end	


    #UNIT
 	 
 	  test "material should have unit" do
  		material = Material.new(:unit => "unidade")

  		assert material.save(validate: false), "It was not possible to insert a unit"
  	end

  	#test "material unit must not be null" do
    #	my_material = materials(:unit_nil)
    #	assert !my_material.save, "Saved with unit NIL"
  	#end
  
  	#test "material unit must have at least 2 characters" do
    #  	my_material = materials(:short_unit)
    #  	assert !my_material.valid?, "Saved with acronym shorter than 2 characters"
  	#end

  	#test "material unit must have less than 20 characters" do
    #  	my_material = materials(:long_unit)
    #  	assert !my_material.valid?, "Saved with acronym 21-character-long"
  	#end

    test "material unit must not be null" do
      my_material = build(:material, :unit => nil)
      assert !my_material.save, "Saved with unit NIL"
    end
  
    test "material unit must have at least 2 characters" do
        my_material = build(:material, :unit => "a")
        assert !my_material.save, "Saved with acronym shorter than 2 characters"
    end

    test "material unit must have less than 20 characters" do
        my_material = build(:material, :unit => "aaaaaaaaaabbbbbbbbbb1")
        assert !my_material.save, "Saved with acronym 21-character-long"
    end

  
  	#RELACIONAMENTO  
  
  	#test "material sub service order id must not be null" do
    #	my_material = materials(:sub_service_order_id_nil)
    #	assert !my_material.save, "Saved with sub service order NIL"
  	#end

    test "material sub service order id must not be null" do
     my_material = build(:material, :sub_service_order_id => nil)
     assert !my_material.save, "Saved with sub service order NIL"
    end
end
