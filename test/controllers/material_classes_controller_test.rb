require 'test_helper'

class MaterialClassesControllerTest < ActionController::TestCase

  ###################
  #Tests for actions#
  ###################

  def setup
    create(:material_class)
    create(:so)
  end

  #######
  #index#
  #######
  test "should be possible request index action" do
  	get :index

  	assert_response :success, "should be possible to request index action"
  end

  test "action index should retrieve all material classes" do

    get :index

    assert assigns(:material_classes) == MaterialClass.all, "material_classes local variable does not contain all material classes"
  end

  #####
  #new#
  #####

  test "should be possible request new action" do
    get :new

    assert_response :success, "should be possible to request new action"
  end

  test "action new should create an instance variable" do
    get :new

    assert_not_nil assigns(:material_class), "action new does not create an instance variable of type MaterialClass"
  end

  ########
  #create#
  ########

  test "action create really creates a new well-formed MaterialClass based on the form" do
    assert_difference('MaterialClass.count') do
      post :create, material_class: {name: "Name", acronym: "Acronym"}
    end
  end

  test "action create redirects to action index after creating a well-formed MaterialClass based on the form" do
    post :create, material_class: {name: "Name", acronym: "Acronym"}

    assert_redirected_to material_classes_path
  end

  test "action create redirects to action new after creating a badly formed MaterialClass based on the form" do
    post :create, material_class: {acronym: "Acronym"}

    assert_template :new
  end

  test "after a badly formed submission, fields filled correcty must be displayed" do
    post :create, material_class: {acronym: "Acronym"}

    assert_equal assigns(:material_class).acronym, "Acronym"
  end

  #########
  #destroy#
  #########

  test "destroy should delete a tuple without relationship" do
    @material_class = create(:material_class_without_relationship)
    
    assert_difference('MaterialClass.count', -1) do
      delete :destroy, id: @material_class
    end
  end

  test "after calling the destroy action passing an invalid id, you must be redirected to the action index" do
    delete :destroy, id: -1

    assert_redirected_to material_classes_path
  end

  test "destroy can not delete a tuple that has a relationship with so" do
    @material_classes = MaterialClass.all
    found = false

    @material_classes.each do |material_class|
      if(material_class.sos != nil and material_class.sos.length != 0) then
        found = true
        assert_difference('MaterialClass.count', 0) do
          delete :destroy, id: material_class
        end
      end
    end

    assert found, "Your test database does not contain a tuple that is complete enough for this test. Check it and try again."
  end

  ##########
  # edit #
  ##########

  test "should get edit" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class
    assert_response :success
  end
  
  test "after calling edit action passing a valid id, you must be redirected to the view edit.html.erb" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_template :edit
  end

  test "after calling the edit action passing an invalid id, you must be redirected to the action index" do
    get :edit, id: -1

    assert_redirected_to material_classes_path
  end

  test "after calling the edit action, there must be a local variable storing the material class retrieved from database" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_not_nil assigns(:material_class)
  end

  test "after calling the edit action, there must be a coherent local variable storing the material class retrieved from database" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_equal @material_class, assigns(:material_class)
  end

  ##########
  # update #
  ##########

  test "should redirect to action show after updating a valid material class" do
    @material_class = build(:material_class)
    @material_class.save

    patch :update, id: @material_class, material_class: { name: @material_class.name, acronym: @material_class.acronym }
    assert_redirected_to material_classes_path
  end

  test "should update a valid material class correcty" do
    @material_class = build(:material_class)
    @material_class.save

    @material_class.name = "My new name"
    @material_class.acronym = "My new acronym"

    patch :update, id: @material_class, material_class: { name: @material_class.name, acronym: @material_class.acronym }

    @material_class_from_database = MaterialClass.find(@material_class.id)

    assert_equal @material_class_from_database.name, @material_class.name
    assert_equal @material_class_from_database.acronym, @material_class.acronym
  end

  test "update should redirect to index after receiving an invalid id" do
    patch :update, id: -1, material_class: {}
    assert_redirected_to material_classes_path
  end

  test "update should return to edit.html.erb after trying to update an invalid material class" do
    @material_class = build(:material_class)
    @material_class.save

    @material_class.name = nil

    patch :update, id: @material_class, material_class: { name: @material_class.name }

    assert_template :edit
  end
  
  ###################
  #Tests for views###
  ###################

  ################
  #index.html.erb#
  ################

  test "index.html.erb must have a h2" do
    get :index

    assert_select 'h2', 1
  end

  test "index.html.erb h2 must have header MATERIAL CLASSES" do
    get :index

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "MATERIAL CLASSES"}, "There is no header for material classes"
      end
  end
  test "index.html.erb must have a table" do
    get :index

    assert_select 'table', 1
  end

  test "index.html.erb must contain exactly one table with id material_classes" do
    get :index

    assert_select "table" do
      assert_select "[id=?]", "material_classes"
    end  
  end

  test "index.html.erb table must have two headers" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "material_classes" do
        assert_select 'tr' do
          assert_select 'th', 3
        end
      end
    end
  end

  test "index.html.erb table must have header NAME" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "material_classes" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "NAME"}, "There is no header for name"
        end
      end
    end
  end

   test "index.html.erb table must have header ACRONYM" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "material_classes" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "ACRONYM"}, "There is no header for acronym"
        end
      end
    end
  end

  test "index.html.erb table must have header OPTIONS" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "material_classes" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "OPTIONS"}, "There is no header for OPTIONS"
        end
      end
    end
  end

  test "field name of material classes is being displayed on table" do
    get :index
    
    @material_classes = MaterialClass.all

    assert_select "table" do
      assert_select "[id=?]", "material_classes" do
        assert_select "tr" do 
          @material_classes.each do |material_class|
            assert_select "td", {text: material_class.name} do
              assert_select "[id=?]", "material_class_name_"+material_class.id.to_s, {count: 1}
            end
          end
        end
      end
    end

  end

  test "field acronym of material classes is being displayed on table" do
    get :index
    
    @material_classes = MaterialClass.all

    assert_select "table" do
      assert_select "[id=?]", "material_classes" do
        assert_select "tr" do 
          @material_classes.each do |material_class|
            assert_select "td", {text: material_class.acronym} do
              assert_select "[id=?]", "material_class_acronym_"+material_class.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to edit material class" do
    get :index

    @material_classes = MaterialClass.all

    assert_select "table" do
      assert_select "[id=?]", "material_classes" do
        assert_select "tr" do 
          @material_classes.each do |material_class|
            assert_select "td" do
              assert_select "[id=?]", "material_class_edit_"+material_class.id.to_s, {count: 1} do
                assert_select "[href=?]", edit_material_class_path(material_class), {count: 1}
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to create a new material class" do
    get :index

    assert_select "[href=?]", new_material_class_path
  end


  test "index.html.erb must contain a link to destroy a material class" do
    get :index

    @material_classes = MaterialClass.all

    assert_select "table" do
      assert_select "[id=?]", "material_classes" do
        assert_select "tr" do 
          @material_classes.each do |material_class|
            assert_select "td" do
              assert_select "[id=?]", "material_class_destroy_"+material_class.id.to_s, {count: 1} do
                assert_select "[href=?]", material_class_path(material_class) do
                  assert_select "[data-method=?]", "delete"
                end
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to sos" do
    get :index

    assert_select "[href=?]", sos_path
  end

  #######
  # NEW #
  #######

  test "new.html.erb must have a h2" do
    get :new

    assert_select 'h2', 1
  end

  test "new.html.erb h2 must have header Creating a new material class" do
    get :new

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Creating a new material class"}, "There is no header for new material class"
    end
  end

  test "new.html.erb must contain exactly one form" do
    get :new

    assert_select "form", 1
  end

  test "new.html.erb must contain exactly one table" do
    get :new

    assert_select "table", 1
  end

  test "new.html.erb must contain exactly one table with id table_new_material_class" do
    get :new

    assert_select "table" do
      assert_select "[id=?]", "table_new_material_class"
    end  
  end

  test "new.html.erb's form must have action attribute with value /material_classes" do
    get :new

    assert_select "form" do
      assert_select "[action=?]", "/material_classes"
    end
  end

  test "new.html.erb's form must have an attribute method with value post" do
    get :new

    assert_select "form" do
      assert_select "[method=?]", "post"
    end
  end

  test "new.html.erb's form must have one submit button" do
    get :new

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "new.html.erb's form must have one label for name textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "material_class_name", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for name" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_text_field" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "material_class[name]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for acronym textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "material_class_acronym", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for acronym" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "input" do
                assert_select "[type=?]", "text" do
                  assert_select "[name=?]", "material_class[acronym]", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb must contain a link to list all material classes" do
    get :new

    assert_select "[href=?]", material_classes_path
  end

  ###############
  #edit.html.erb#
  ###############

  test "edit.html.erb must have a h2" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select 'h2', 1
  end

  test "edit.html.erb h2 must have header Editing - material class" do
    @material_class = build(:material_class)
    @material_class.save
    
    get :edit, id: @material_class

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Editing - material class"}, "There is no header for edit material class"
      end
  end

  test "edit.html.erb must contain exactly one form" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form", 1
  end

  test "edit.html.erb must contain exactly one table" do
    @material_class = build(:material_class)
    @material_class.save
    
    get :edit, id: @material_class

    assert_select "table", 1
  end

  test "edit.html.erb must contain exactly one table with id table_edit_material_class" do
    @material_class = build(:material_class)
    @material_class.save
    
    get :edit, id: @material_class

    assert_select "table" do
      assert_select "[id=?]", "table_edit_material_class"
    end  
  end

  test "edit.html.erb's form must have action attribute with value /material_classes/id" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form" do
      assert_select "[action=?]", material_class_path(@material_class)
    end
  end

  test "edit.html.erb's form must have one submit button" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "edit.html.erb's form must have one label for name textfield" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "material_class_name", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for name" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_text_field" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "material_class[name]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for acronym" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "material_class_acronym", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for acronym" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_material_class" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_text_field" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "material_class[acronym]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb must contain a link to list all material classes" do
    @material_class = build(:material_class)
    @material_class.save

    get :edit, id: @material_class

    assert_select "[href=?]", material_classes_path
  end
  
end

