require 'test_helper'

class SubServiceOrdersControllerTest < ActionController::TestCase
  ###################
	#Tests for actions#
	###################

  def setup
    create(:material)

    @so = So.first
    @section_military_organization = SectionMilitaryOrganization.first
    @sub_service_order = SubServiceOrder.first
  end

	#####
	#new#
	#####

	test "should be possible request new action" do

    get :new, {so_id: @so}

    assert_response :success, "should be possible to request new action"

	end

	test "action new should create an instance variable" do
  	get :new, {so_id: @so}
    assert_not_nil assigns(:sub_service_order), "action new does not create an instance variable of type SubServiceOrder"
	end

  test "after calling the new action passing an invalid id the so, you must be redirected to the action index the so " do
    get :new, {so_id: -1}

    assert_redirected_to sos_path
  end

	########
	#create#
	########

	test "action create really creates a new well-formed SubServiceOrder based on the form" do
  	assert_difference('SubServiceOrder.count') do
    		post :create, so_id: @so, sub_service_order: {description: "Description", amount: "10", service_description: "ServiceDescription", technical: "CB fulano", so_id: @so, section_military_organization_id: @section_military_organization}
  	end
	end

	test "action create redirects to action show after creating a well-formed SubServiceOrder based on the form" do
  	post :create, so_id: @so, sub_service_order: {description: "Description", amount: "10", service_description: "ServiceDescription", technical: "CB fulano", so_id: @so, section_military_organization_id: @section_military_organization}
  	assert_redirected_to so_path(@so)
	end

	test "action create redirects to action new after creating a badly formed SubServiceOrder based on the form" do
  	post :create, so_id: @so, sub_service_order: {description: "Description"}
  	assert_template :new
	end

	test "after a badly formed submission, fields filled correcty must be displayed" do
  	post :create, so_id: @so, sub_service_order: {description: "Description"}
  	assert_equal assigns(:sub_service_order).description, "Description"
	end

  test "after calling the create action passing an invalid id the so, you must be redirected to the action index the so " do
    post :create, so_id: -1, sub_service_order: {description: "Description", amount: "10", service_description: "ServiceDescription", technical: "CB fulano", so_id: @so, section_military_organization_id: @section_military_organization}

    assert_redirected_to sos_path
  end

	######
	#show#
	######

	test "internal variable of show action must not be nil" do
  	
  	get :show, so_id:@so, id: @sub_service_order

  	assert_not_nil assigns(:sub_service_order)
	end

  test "internal variable of show action must be coherent" do
    
    get :show, so_id:@so, id: @sub_service_order

    assert @sub_service_order == assigns(:sub_service_order), "Content of internal variable Sub Service Order is not valid"
  end

  test "after calling the show action passing an invalid id, you must be redirected to the action show the service order " do
    get :show, so_id: @so, id: -1

    assert_redirected_to so_path(@so)
  end

  test "after calling the show action passing an invalid id the so, you must be redirected to the action index the so " do
    get :show, so_id: -1, id: @sub_service_order

    assert_redirected_to sos_path
  end

  #########
  #destroy#
  #########

  test "destroy should delete a valid tuple" do
    @sub_service_order = build(:sub_service_order)
    @sub_service_order.save
  
    assert_difference('SubServiceOrder.count', -1) do
      delete :destroy, so_id: @so, id: @sub_service_order
    end
  end

  test "after calling the destroy action passing an invalid id, you must be redirected to the action show the SO" do
    delete :destroy, so_id: @so, id: -1

    assert_redirected_to so_path(@so)
  end

  test "after calling the destroy action passing an invalid id the so, you must be redirected to the action index the so " do
    delete :destroy, so_id: -1, id: @sub_service_order

    assert_redirected_to sos_path
  end

  ##########
  # edit   #
  ##########

  test "should get edit" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_response :success
  end

  test "after calling edit action passing a valid id, you must be redirected to the view edit.html.erb" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_template :edit
  end

  test "after calling the edit action passing an invalid id, you must be redirected to the action show the SO" do
    get :edit, so_id: @so, id: -1

    assert_redirected_to so_path(@so)
  end

  test "after calling the edit action passing an invalid id the so, you must be redirected to the action index the so " do
    get :edit, so_id: -1, id: @sub_service_order

    assert_redirected_to sos_path
  end

  test "after calling the edit action, there must be a local variable storing the Sub Service Order retrieved from database" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_not_nil assigns(:sub_service_order)
  end

  test "after calling the edit action, there must be a coherent local variable storing the Sub Service Order retrieved from database" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_equal @sub_service_order, assigns(:sub_service_order)
  end

  ##########
  # update #
  ##########

  test "should redirect to action show after updating a valid Sub Service Order" do
    patch :update, so_id: @so, id: @sub_service_order, sub_service_order: { description: @sub_service_order.description, amount: @sub_service_order.amount, service_description: @sub_service_order.service_description, technical: @sub_service_order.technical, so_id: @so, section_military_organization_id: @section_military_organization}
    
    assert_redirected_to so_path(@so)
  end

  test "should update a valid Sub Service Order correcty" do
    @sub_service_order.description = "My new description"

    patch :update, so_id: @so, id: @sub_service_order, sub_service_order: { description: @sub_service_order.description, amount: @sub_service_order.amount, service_description: @sub_service_order.service_description, technical: @sub_service_order.technical, so_id: @so, section_military_organization_id: @section_military_organization}

    @sub_service_order_from_database = SubServiceOrder.find(@sub_service_order.id)

    assert_equal @sub_service_order_from_database.description, @sub_service_order.description
  end

  test "update should redirect to show the SO after receiving an invalid id" do
    patch :update, so_id: @so, id: -1, sub_service_order: {description: "Empty"}
    assert_redirected_to so_path(@so)
  end

  test "update should redirect to index the SO after receiving an invalid so id" do
    patch :update, so_id: -1, id: @sub_service_order, sub_service_order: {description: "Empty"}
    assert_redirected_to sos_path
  end

  test "update should return to edit.html.erb after trying to update an invalid Sub Service Orders" do
    @sub_service_order.description = nil

    patch :update, so_id: @so, id: @sub_service_order, sub_service_order: { description: @sub_service_order.description }

    assert_template :edit
  end

  #################
  #Tests for views#
  #################

  ################
  #new.html.erb#
  ################

  test "new.html.erb must have a h2" do
    get :new, so_id: @so

    assert_select 'h2', 1
  end

  test "new.html.erb h2 must have header Creating a new sub service order" do
    get :new, so_id: @so

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Creating a new sub service order"}, "There is no header for sub service order"
      end
  end

  test "new.html.erb must contain exactly one form" do
    get :new, so_id: @so

    assert_select "form", 1
  end

  test "new.html.erb must contain exactly one table" do
    get :new, so_id: @so

    assert_select "table", 1
  end

  test "new.html.erb must contain exactly one table with id sub_service_order" do
    get :new, so_id: @so

    assert_select "table" do
      assert_select "[id=?]", "sub_service_order"
    end  
  end

  test "new.html.erb's form must have action attribute with value /sub_service_orders" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "[action=?]", so_sub_service_orders_path(@so)
    end
  end

  test "new.html.erb's form must have an attribute method with value post" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "[method=?]", "post"
    end
  end

  test "new.html.erb's form must have one submit button" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "new.html.erb's form must have one label for SO guide number textfield" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "so_guide_number" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_so_guide_number", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one td for so guide number" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "so_guide_number" do
              assert_select "td", {text:@sub_service_order.so.guide_number, count: 1}
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have a hidden field with so_id" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "so_guide_number" do 
              assert_select "input" do
                assert_select "[type=?]", "hidden" do
                  assert_select "[value=?]", @so.id, {count: 1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for description textarea" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "description" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_description", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textarea for description" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "description" do
              assert_select "td" do
                assert_select "textarea" do
                  assert_select "[name=?]", "sub_service_order[description]", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for amount textfield" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "amount" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_amount", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for amount" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "amount" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "sub_service_order[amount]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for service description textarea" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "service_description" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_service_description", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textarea for service description" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "service_description" do
              assert_select "td" do
                assert_select "textarea" do
                  assert_select "[name=?]", "sub_service_order[service_description]", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for technical textfield" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "technical" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_technical", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for technical" do
    get :new, so_id: @so

   assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "technical" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "sub_service_order[technical]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for section military organization id select" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "section_military_organization" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_section_military_organization_id", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one select for section military organization id" do
    get :new, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "section_military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "sub_service_order[section_military_organization_id]", {count:1}
                end 
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one select showing section military organization id correcty" do
    get :new, so_id: @so

    @section_military_organizations = SectionMilitaryOrganization.all

    @section_military_organizations.each do |section_military_organization|
      assert_select "form" do
        assert_select "table" do
          assert_select "[id=?]", "sub_service_order" do
            assert_select "tr" do
              assert_select "[id=?]", "section_military_organization" do
                assert_select "td" do
                  assert_select "select" do
                    assert_select "[name=?]", "sub_service_order[section_military_organization_id]", {count:1} do
                      assert_select "option" do
                        assert_select "[value=?]", section_military_organization.id, {count:1}
                      end
                    end
                  end 
                end
              end
            end
          end
        end
      end
    end  
  end

  test "new.html.erb's form must have one select showing section military organization name correcty" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.name = "6cta"
    @section_military_organization.save

    get :new, so_id: @so
    
    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "section_military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "sub_service_order[section_military_organization_id]", {count:1} do
                    assert_select 'option', {count: 1, text: @section_military_organization.name}, "section military organization name is not being displayed"
                  end
                end 
              end
            end
          end
        end
      end
    end 
  end

  test "new.html.erb must contain a link to list all sub service orders" do
    get :new, so_id: @so

    assert_select "[href=?]", so_path(@so)
  end



  ################
  #show.html.erb#
  ################

  test "show.html.erb must have a h2" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'h2', 1
  end

  test "show.html.erb h2 must have header Showing Sub Service Order" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Showing Sub Service Order"}, "There is no header for show sub service order"
      end
  end

  test "show.html.erb must contain exactly two table" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select "table", 3
  end

  test "show.html.erb table must have header DESCRIPTION" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "DESCRIPTION"}, "There is no header for description"
          end
        end
      end
    end
  end

   test "show.html.erb table must have header AMOUNT" do
    
    get :show, so_id: @so, id: @sub_service_order
    
    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "AMOUNT"}, "There is no header for amount"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header SERVICE DESCRIPTION" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "SERVICE DESCRIPTION"}, "There is no header for service description"
          end
        end
      end
    end
  end

   test "show.html.erb table must have header TECHNICAL" do
    
    get :show, so_id: @so, id: @sub_service_order
    
    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "TECHNICAL"}, "There is no header for technical"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header GUIDE NUMBER(SO)" do
    
    get :show, so_id: @so, id: @sub_service_order
    
    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "GUIDE NUMBER(SO)"}, "There is no header for guide number this so"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header SECTION MILITARY ORGANIZATION" do
    
    get :show, so_id: @so, id: @sub_service_order
    
    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "SECTION MILITARY ORGANIZATION"}, "There is no header for section military organization"
          end
        end
      end
    end
  end

  test "show.html.erb must display the field descrition of a sub service order inside a td" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "information" do
            assert_select "[id=?]", "description", {count: 1} do
              assert_select 'td', {text: @sub_service_order.description}, "Description is not being displayed correctly"
            end
          end
        end
      end  
    end
  end

  test "show.html.erb must display the field amount of a sub service order inside a td" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "information" do
            assert_select "[id=?]", "amount", {count: 1} do
              assert_select 'td', {text: @sub_service_order.amount}, "Amount is not being displayed correctly"
            end
          end
        end
      end  
    end
  end

  test "show.html.erb must display the field service description of a sub service order inside a td" do
    
    get :show, so_id: @so, id: @sub_service_order

    
    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "information" do
            assert_select "[id=?]", "service_description", {count: 1} do
              assert_select 'td', {text: @sub_service_order.service_description}, "Service Description is not being displayed correctly"
            end
          end
        end
      end  
    end
  end

  test "show.html.erb must display the field technical of a sub service order inside a td" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "information" do
            assert_select "[id=?]", "technical", {count: 1} do
              assert_select 'td', {text: @sub_service_order.technical}, "Technical is not being displayed correctly"
            end
          end
        end
      end  
    end
  end

  test "show.html.erb must display the field guide number this so of a sub service order inside a td" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "information" do
            assert_select "[id=?]", "so_guide_number", {count: 1} do
              assert_select 'td', {text: @sub_service_order.so.guide_number}, "So Guide Number is not being displayed correctly"
            end
          end
        end
      end  
    end
  end

  test "show.html.erb must display the field section military organization acronym of a so inside a td" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "sub_service_order" do
        assert_select 'tr' do
          assert_select "[id=?]", "information" do
            assert_select "[id=?]", "section_military_organization_acronym", {count: 1} do
              assert_select 'td', {text: @sub_service_order.section_military_organization.acronym}, "Amount is not being displayed correctly"
            end
          end
        end
      end  
    end
  end

  test "show.html.erb must contain a link to return to sub_service_order" do
    get :show, so_id: @sub_service_order.so, id: @sub_service_order

    assert_select "[href=?]", so_path(@sub_service_order.so)
  end

  test "show.html.erb must contain a link to add materials" do
    get :show, so_id: @sub_service_order.so, id: @sub_service_order

    assert_select "[href=?]", new_so_sub_service_order_material_path(@so,@sub_service_order)
  end

  # validando materials

  test "show.html.erb must have a h3" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'h3', 1
  end

  test "show.html.erb h3 must have header Showing Materials" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'h3' do
        assert_select 'b', {count: 1, text: "Showing Materials"}, "There is no header for show materials"
      end
  end

  test "show.html.erb table must have header NAME" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "materials" do
        assert_select 'tr' do
          assert_select "[id=?]", "header_material" do
            assert_select 'th', {count: 1, text: "NAME"}, "There is no header for name"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header AMOUNTS this material" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "materials" do
        assert_select 'tr' do
          assert_select "[id=?]", "header_material" do
            assert_select 'th', {count: 1, text: "AMOUNTS"}, "There is no header for amount"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header VALUE" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "materials" do
        assert_select 'tr' do
          assert_select "[id=?]", "header_material" do
           assert_select 'th', {count: 1, text: "VALUE"}, "There is no header for value"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header UNIT" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "materials" do
        assert_select 'tr' do
          assert_select "[id=?]", "header_material" do
            assert_select 'th', {count: 1, text: "UNIT"}, "There is no header for unit"
          end
        end
      end
    end
  end

  test "show.html.erb table must have header OPTION" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "materials" do
        assert_select 'tr' do
          assert_select "[id=?]", "header_material" do
            assert_select 'th', {count: 1, text: "OPTION"}, "There is no header for option"
          end
        end
      end
    end
  end

  test "show.html.erb must contain a column listing the name of each material" do
    get :show, so_id: @so, id: @sub_service_order

    assert_select "table" do
        assert_select "tr" do
          @sub_service_order.materials.each do |material|
            assert_select "td", {text: material.name} do
              assert_select "[id=?]", "material_name_"+material.id.to_s, {count: 1}
            end
          end
        end
    end
  end

  test "show.html.erb must contain a column listing the amount of each material" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select "table" do
      assert_select "[id=?]", "materials" do
        assert_select "tr" do 
          @sub_service_order.materials.each do |material|
            assert_select "td", {text: material.amount} do
              assert_select "[id=?]", "material_amount_"+material.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a column listing the value of each material" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select "table" do
      assert_select "[id=?]", "materials" do
        assert_select "tr" do 
          @sub_service_order.materials.each do |material|
            assert_select "td", {text: material.value} do
              assert_select "[id=?]", "material_value_"+material.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a column listing the unit of each material" do
   
    get :show, so_id: @so, id: @sub_service_order

    assert_select "table" do
      assert_select "[id=?]", "materials" do
        assert_select "tr" do 
          @sub_service_order.materials.each do |material|
            assert_select "td", {text: material.unit} do
              assert_select "[id=?]", "material_unit_"+material.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "show.html.erb table must have header TOTAL" do
    
    get :show, so_id: @so, id: @sub_service_order

    assert_select 'table' do
      assert_select "[id=?]", "table_total" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "TOTAL:"}, "There is no header for total"
        end
      end
    end
  end

  test "show.html.erb must display the field total value inside a td" do
   
    get :show, so_id: @so, id: @sub_service_order

    @total = 0
    @sub_service_order.materials.each do |material|
      @total += (material.amount * material.value)
    end
    
    assert_select 'table' do
      assert_select "[id=?]", "table_total" do
        assert_select 'tr' do
          assert_select 'td', {count: 1, text: "R$ "+@total.to_s}, "Total Value is not being displayed correctly"
        end
      end
    end
  end

  ###############
  #edit.html.erb#
  ###############

  test "edit.html.erb must have a h2" do
    get :edit, so_id: @so, id: @sub_service_order
    
    assert_select 'h2', 1
  end

  test "edit.html.erb h2 must have header Editing - Sub Service Order" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Editing - Sub Service Order"}, "There is no header for edit sub service order"
      end
  end

  test "edit.html.erb must contain exactly one form" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form", 1
  end

  test "edit.html.erb must contain exactly one table" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "table", 1
  end

  test "edit.html.erb must contain exactly one table with id table_edit_so" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "table" do
      assert_select "[id=?]", "sub_service_order"
    end  
  end

  test "edit.html.erb's form must have action attribute with value /sub_service_orders/id" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "[action=?]", so_sub_service_order_path(@so, @sub_service_order)
    end
  end

  test "edit.html.erb's form must have one submit button" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "edit.html.erb's form must have one td for so guide number" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td", {text:@sub_service_order.so.guide_number, count: 1}
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for description textfield" do
    get :edit, so_id: @so, id: @sub_service_order

    
    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_so_guide_number", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textarea for description" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "description" do
              assert_select "td" do
                assert_select "textarea" do
                  assert_select "[name=?]", "sub_service_order[description]", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for amount textfield" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "amount" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_amount", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for amount" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "amount" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "sub_service_order[amount]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for service description textfield" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "service_description" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_service_description", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textarea for service description" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "service_description" do
              assert_select "td" do
                assert_select "textarea" do
                  assert_select "[name=?]", "sub_service_order[service_description]", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for technical textfield" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "technical" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_technical", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for technical" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "technical" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "sub_service_order[technical]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for so_guide_number textfield" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_so_guide_number", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for section military organization id" do

    get :edit, id: @sub_service_order, so_id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "section_military_organization" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "sub_service_order_section_military_organization_id", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one select for section military organization id" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "section_military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "sub_service_order[section_military_organization_id]", {count:1}
                end 
              end
            end
          end
        end
      end
    end  
  end

  test "edit.html.erb's form must have one select showing section military organization id correcty" do
    get :edit, so_id: @so, id: @sub_service_order

    @section_military_organizations = SectionMilitaryOrganization.all

    @section_military_organizations.each do |section_military_organization|
      assert_select "form" do
        assert_select "table" do
          assert_select "[id=?]", "sub_service_order" do
            assert_select "tr" do
              assert_select "[id=?]", "section_military_organization" do
                assert_select "td" do
                  assert_select "select" do
                    assert_select "[name=?]", "sub_service_order[section_military_organization_id]", {count:1} do
                      assert_select "option" do
                        assert_select "[value=?]", section_military_organization.id, {count:1}
                      end
                    end
                  end 
                end
              end
            end
          end
        end
      end
    end 
  end

  test "edit.html.erb's form must have one select showing all section military organization names correctly" do
    section_military_organizations = SectionMilitaryOrganization.all

    get :edit, so_id: @so, id: @sub_service_order
    
    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "section_military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "sub_service_order[section_military_organization_id]", {count:1} do
                    section_military_organizations.each do |section_military_organization|
                      assert_select 'option', {count: 1, text: section_military_organization.name}, "section military organization name is not being displayed"
                    end 
                  end
                end
              end
            end
          end
        end
      end 
    end 
  end

  

  test "edit.html.erb's form must have a hidden field with so_id" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "sub_service_order" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "input" do
                assert_select "[type=?]", "hidden" do
                  assert_select "[value=?]", @so.id, {count: 1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb must contain a link to list all sub service orders the so" do
    get :edit, so_id: @so, id: @sub_service_order

    assert_select "[href=?]", so_path(@so)
  end
end

