require 'test_helper'

class SosControllerTest < ActionController::TestCase

  ###################
  #Tests for actions#
  ###################

  #######
  #index#
  #######

  def setup
    create(:sub_service_order)
  end

  test "should be possible request index action" do
  	get :index

  	assert_response :success, "should be possible to request index action"
  end

  test "action index should retrieve all service orders" do

  	get :index

    assert assigns(:sos) == So.all, "sos local variable does not contain all service orders"
  end

  #####
  #new#
  #####

  test "should be possible request new action" do
    get :new

    assert_response :success, "should be possible to request new action"
  end

  test "action new should create an instance variable" do

    get :new

    assert_not_nil assigns(:so), "action new does not create an instance variable of type So"
  end

  ########
  #create#
  ########

  test "action create really creates a new well-formed SO based on the form" do
    assert_difference('So.count') do
       post :create, so: {material_description: 'Material description', guide_number: 123, sender_name: "Oioioi", sender_phone: "(67)1234-1123", total_amount: 1, material_class_id: 1, military_organization_id: 1}
    end
  end

  test "action create redirects to index after creating a well-formed MaterialClass based on the form" do
    post :create, so: {material_description: 'Material description', guide_number: 123, sender_name: "Oioioi", sender_phone: "(67)1234-1123", total_amount: 1, material_class_id: 1, military_organization_id: 1}

    assert_redirected_to sos_path
  end

  test "action create redirects to action new after creating a badly formed MaterialClass based on the form" do
    post :create, so: {sender_name: "fulano"}

    assert_template :new
  end

  test "after a badly formed submission, fields filled correcty must be displayed" do
    post :create, so: {sender_name: "fulano"}

    assert_equal assigns(:so).sender_name, "fulano"
  end

  ######
  #show#
  ######

  test "internal variable of show action must not be nil" do
    @so = build(:so)
    @so.save

    get :show, id: @so

    assert_not_nil assigns(:so)
  end

  test "internal variable of show action must be coherent" do
    @so = build(:so)
    @so.save

    get :show, id: @so

    assert @so == assigns(:so), "Content of internal variable SO is not valid"
  end

  test "after calling the show action passing an invalid id, you must be redirected to the action index" do
    get :show, id: -1

    assert_redirected_to sos_path
  end

  #########
  #destroy#
  #########

  test "destroy should delete a valid tuple" do
    @so = build(:so)
    @so.save
    
    assert_difference('So.count', -1) do
      delete :destroy, id: @so
    end
  end

  test "after calling the destroy action passing an invalid id, you must be redirected to the action index" do
    delete :destroy, id: -1

    assert_redirected_to sos_path
  end

  ##########
  # edit #
  ##########

  test "should get edit" do
    @so = build(:so)
    @so.save

    get :edit, id: @so
    assert_response :success
  end

  test "after calling edit action passing a valid id, you must be redirected to the view edit.html.erb" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_template :edit
  end

  test "after calling the edit action passing an invalid id, you must be redirected to the action index" do
    get :edit, id: -1

    assert_redirected_to sos_path
  end

  test "after calling the edit action, there must be a local variable storing the SO retrieved from database" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_not_nil assigns(:so)
  end

  test "after calling the edit action, there must be a coherent local variable storing the SO retrieved from database" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_equal @so, assigns(:so)
  end

  ##########
  # update #
  ##########

  test "should redirect to action index after updating a valid SO" do
    @so = build(:so)
    @so.save

    patch :update, id: @so, so: { material_description: @so.material_description, guide_number: @so.guide_number, sender_name: @so.sender_name, sender_phone: @so.sender_phone, total_amount: @so.total_amount, material_class_id: @so.material_class_id, military_organization_id: @so.military_organization_id}
    assert_redirected_to sos_path
  end
 
 test "should update a valid SO correcty" do
   @so = build(:so)
   @so.save

   @so.material_description = "My new material description"

   patch :update, id: @so, so: { material_description: @so.material_description}

   @so_from_database = So.find(@so.id)

   assert_equal @so_from_database.material_description, @so.material_description
 end

  test "update should redirect to index after receiving an invalid id" do
    patch :update, id: -1, so: {}
    assert_redirected_to sos_path
  end

  test "update should return to edit.html.erb after trying to update an invalid SO" do
    @so = build(:so)
    @so.save

    @so.material_description = nil

    patch :update, id: @so, so: { material_description: @so.material_description }

    assert_template :edit
  end
  
  #################
  #Tests for views#
  #################

  ################
  #index.html.erb#
  ################
  test "index.html.erb must have a h2" do
    get :index

    assert_select 'h2', 1
  end
  test "index.html.erb h2 must have header SERVICE ORDERS" do
    get :index

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "SERVICE ORDERS"}, "There is no header for service orders"
      end
  end
  test "index.html.erb must have a table" do
    get :index

    assert_select 'table', 1
  end

  test "index.html.erb must contain exactly one table with id sos" do
    get :index

    assert_select "table" do
      assert_select "[id=?]", "sos"
    end  
  end

  test "index.html.erb table must have three headers" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "sos" do
        assert_select 'tr' do
          assert_select 'th', 3
        end
      end
    end
  end

  test "index.html.erb table must have header MATERIAL DESCRIPTION" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "sos" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "MATERIAL DESCRIPTION"}, "There is no header for material description"
        end
      end
    end
  end

   test "index.html.erb table must have header GUIDE NUMBER" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "sos" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "GUIDE NUMBER"}, "There is no header for guide number"
        end
      end
    end
  end

  test "index.html.erb table must have header OPTIONS" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "sos" do
        assert_select 'tr' do
          assert_select 'th', {count: 1, text: "OPTIONS"}, "There is no header for OPTIONS"
        end
      end
    end
  end

  test "field material description of SOs is being displayed on table" do
    get :index
    
    @sos = So.all

    assert_select "table" do
      assert_select "[id=?]", "sos" do
        assert_select "tr" do 
          @sos.each do |so|
            assert_select "td", {text: so.material_description} do
              assert_select "[id=?]", "so_material_description_"+so.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "field guide number of SOs is being displayed on table sos" do
    get :index
    
    @sos = So.all

     assert_select "table" do
      assert_select "[id=?]", "sos" do
        assert_select "tr" do 
          @sos.each do |so|
            assert_select "td", {text: so.guide_number} do
              assert_select "[id=?]", "so_guide_number_"+so.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to show so" do

    get :index

    @sos = So.all

    assert_select "table" do
      assert_select "[id=?]", "sos" do
        assert_select "tr" do 
          @sos.each do |so|
            assert_select "td" do
              assert_select "[id=?]", "so_show_"+so.id.to_s, {count: 1} do
                assert_select "[href=?]", so_path(so), {count: 1, text: "Show"}
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to edit so" do
    get :index

    @sos = So.all

    assert_select "table" do
      assert_select "[id=?]", "sos" do
        assert_select "tr" do 
          @sos.each do |so|
            assert_select "td" do
              assert_select "[id=?]", "so_edit_"+so.id.to_s, {count: 1} do
                assert_select "[href=?]", edit_so_path(so) , {count: 1}
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to create a new so" do
    get :index

    assert_select "[href=?]", new_so_path 
  end

  test "index.html.erb must contain a link to destroy a so" do
    get :index

    @sos = So.all

    assert_select "table" do
      assert_select "[id=?]", "sos" do
        assert_select "tr" do 
          @sos.each do |so|
            assert_select "td" do
              assert_select "[id=?]", "so_destroy_"+so.id.to_s, {count: 1} do
                assert_select "[data-method=?]", "delete" do
                  assert_select "[href=?]", so_path(so), {count: 1, text: "Destroy"}
                end
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to manage material classes" do
    get :index
    assert_select "[href=?]", material_classes_path
  end

  test "index.html.erb must contain a link to manage military organizations" do
    get :index
    assert_select "[href=?]", military_organizations_path
  end

  test "index.html.erb must contain a link to manage section military organizations" do
    get :index

    assert_select "[href=?]", section_military_organizations_path
  end

  ################
  #new.html.erb#
  ################

  test "new.html.erb must have a h2" do
    get :new

    assert_select 'h2', 1
  end

  test "new.html.erb h2 must have header Creating a new service order" do
    get :new

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Creating a new service order"}, "There is no header for service order"
      end
  end

  test "new.html.erb must contain exactly one form" do
    get :new

    assert_select "form", 1
  end

  test "new.html.erb must contain exactly one table" do
    get :new
    assert_select "table", 1
  end

  test "new.html.erb must contain exactly one table with id table_new_so" do
    get :new

    assert_select "table" do
      assert_select "[id=?]", "table_new_so"
    end  
  end

  test "new.html.erb's form must have action attribute with value /sos" do
    get :new

    assert_select "form" do
      assert_select "[action=?]", "/sos"
    end
  end

  test "new.html.erb's form must have an attribute method with value post" do
    get :new

    assert_select "form" do
      assert_select "[method=?]", "post"
    end
  end

  test "new.html.erb's form must have one submit button" do
    get :new

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "new.html.erb's form must have one label for material description textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_description" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_material_description", {count:1}
                end
              end
            end
          end
        end
      end      
    end
  end

  test "new.html.erb's form must have one textfield for material description" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_description" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[material_description]", {count:1}
                  end
                end
              end
            end
          end
        end
      end    
    end
  end

  test "new.html.erb's form must have one label for guide number textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_guide_number", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for guide number" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[guide_number]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for sender name" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_name" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_sender_name", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for sender name" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_name" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[sender_name]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for sender phone" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_phone" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_sender_phone", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for sender phone" do
    get :new

   assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_phone" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[sender_phone]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for total amount" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "total_amount" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_total_amount", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for total amount" do
    get :new

   assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "total_amount" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[total_amount]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for material class id select" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_material_class_id", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one select for material class id" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[material_class_id]", {count:1}
                end
              end
            end
          end
        end
      end 
    end
  end

  test "new.html.erb's form must have one select showing material_class_id correcty" do
    get :new

    @material_classes = MaterialClass.all

    @material_classes.each do |material_class|
      assert_select "form" do
        assert_select "table" do
          assert_select "[id=?]", "table_new_so" do
            assert_select "tr" do
              assert_select "[id=?]", "material_class" do
                assert_select "td" do
                  assert_select "select" do
                    assert_select "[name=?]", "so[material_class_id]", {count:1} do
                      assert_select "option" do
                        assert_select "[value=?]", material_class.id, {count:1}
                      end
                    end
                  end 
                end
              end
            end
          end
        end
      end
    end 
  end

  test "new.html.erb's form must have one select showing material_class name correcty" do
    @material_class = build(:material_class)
    @material_class.name = "jaoisdjaoisjdoisajdoisajd"
    @material_class.save

    get :new
    
    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[material_class_id]", {count:1} do
                    assert_select 'option', {count: 1, text: @material_class.name}, "material class name is not being displayed"
                  end
                end 
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for military organization id select" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_military_organization_id", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one select for military organization id" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[military_organization_id]", {count:1}
                end
              end
            end
          end
        end
      end 
    end 
  end

  test "new.html.erb's form must have one select showing military_organization_id correcty" do
    get :new

    @military_organizations = MilitaryOrganization.all

    @military_organizations.each do |military_organization|
      assert_select "form" do
        assert_select "table" do
          assert_select "[id=?]", "table_new_so" do
            assert_select "tr" do
              assert_select "[id=?]", "military_organization" do
                assert_select "td" do
                  assert_select "select" do
                    assert_select "[name=?]", "so[military_organization_id]", {count:1} do
                      assert_select "option" do
                        assert_select "[value=?]", military_organization.id, {count:1}
                      end
                    end
                  end 
                end
              end
            end
          end
        end
      end
    end   
  end

  test "new.html.erb's form must have one select showing military organization name correcty" do
    @military_organization = build(:military_organization)
    @military_organization.name = "14ciape"
    @military_organization.save

    get :new
    
    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[military_organization_id]", {count:1} do
                    assert_select 'option', {count: 1, text: @military_organization.name}, "military organization name is not being displayed"
                  end
                end 
              end
            end
          end
        end
      end
    end
    
    
  end

  test "new.html.erb must contain a link to list all sos" do
    get :new

    assert_select "[href=?]", sos_path
  end

  ################
  #show.html.erb#
  ################

  test "show.html.erb must have a h2" do
    @so= So.first
    get :show, id: @so

    assert_select 'h2', 1
  end

  test "show.html.erb h2 must have header Showing Service Order" do
    @so= So.first
    get :show, id: @so

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Showing Service Order"}, "There is no header for show service order"
      end
  end

  test "show.html.erb must have a h3" do
    @so= So.first
    get :show, id: @so

    assert_select 'h3', 1
  end

  test "show.html.erb h3 must have header Showing Sub Service Orders" do
    @so= So.first
    get :show, id: @so

    assert_select 'h3' do
        assert_select 'b', {count: 1, text: "Showing Sub Service Orders"}, "There is no header for show service order"
      end
  end

  test "show.html.erb must contain exactly two table" do
    @so= So.first
    get :show, id: @so

    assert_select "table", 2
  end

  test "show.html.erb table must have header MATERIAL DESCRIPTION" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "MATERIAL DESCRIPTION"}, "There is no header for material description"
      end
    end
  end

   test "show.html.erb table must have header GUIDE NUMBER" do
    @so= So.first
    get :show, id: @so
    
    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "GUIDE NUMBER"}, "There is no header for guide number"
      end
    end
  end

  test "show.html.erb table must have header SENDER NAME" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "SENDER NAME"}, "There is no header for sender name"
      end
    end
  end

   test "show.html.erb table must have header SENDER PHONE" do
    @so= So.first
    get :show, id: @so
    
    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "SENDER PHONE"}, "There is no header for sender phone"
      end
    end
  end

  test "show.html.erb table must have header TOTAL AMOUNT" do
    @so= So.first
    get :show, id: @so
    
    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "TOTAL AMOUNT"}, "There is no header for total amount"
      end
    end
  end

  test "show.html.erb table must have header MATERIAL CLASS" do
    @so= So.first
    get :show, id: @so
    
    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "MATERIAL CLASS"}, "There is no header for material class"
      end
    end
  end

  test "show.html.erb table must have header MILITARY ORGANIZATION" do
    @so= So.first
    get :show, id: @so
    
    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "MILITARY ORGANIZATION"}, "There is no header for military organization"
      end
    end
  end

  test "show.html.erb must display the field material description of a so inside a td" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
          assert_select "[id=?]", "material_description", {count: 1} do
            assert_select 'td', {text: @so.material_description}, "Material Description is not being displayed correctly"
          end
        end
      end  
    end
  end

  test "show.html.erb must display the field guide_number of a so inside a td" do
    @so = So.first

    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
          assert_select "[id=?]", "guide_number", {count: 1} do
            assert_select 'td', {count: 1, text: @so.guide_number}, "Guide number is not being displayed correctly"
          end  
        end
      end  
    end
  end

  test "show.html.erb must display the field sender name of a so inside a td" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
          assert_select "[id=?]", "sender_name", {count: 1} do
            assert_select 'td', {count: 1, text: @so.sender_name}, "sender_name is not being displayed correctly"
          end      
        end
      end
    end
  end

  test "show.html.erb must display the field sender phone of a so inside a td" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
          assert_select "[id=?]", "sender_phone", {count: 1} do
            assert_select "td", {count: 1, text: @so.sender_phone}, "sender_phone is not being displayed correctly"
          end
        end
      end
    end
  end

  test "show.html.erb must display the field total amount of a so inside a td" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
          assert_select "[id=?]", "total_amount", {count: 1} do
            assert_select "td", {count: 1, text: @so.total_amount}, "total_amount is not being displayed correctly"  
          end
        end
      end
    end
  end

  test "show.html.erb must display the field material class name of a so inside a td" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
           assert_select "[id=?]", "material_class_name", {count: 1} do
            assert_select "td", {count: 1, text: @so.material_class.name}, "Material Class Name is not being displayed correctly"
          end  
        end
      end
    end
  end

  test "show.html.erb must display the field military organization acronym of a so inside a td" do
    @so = So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select "[id=?]", "so" do
        assert_select 'tr' do
          assert_select "[id=?]", "military_organization_acronym", {count: 1} do
            assert_select 'td', {count: 1, text: @so.military_organization.acronym}, "MilitaryOrganization acronym is not being displayed correctly"
          end  
        end
      end
    end
  end



  #validando table subservice orders

  test "show.html.erb table must have header DESCRIPTION" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "DESCRIPTION"}, "There is no header for description"
      end
    end
  end

  test "show.html.erb table must have header AMOUNT" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "AMOUNT"}, "There is no header for amount"
      end
    end
  end

  test "show.html.erb table must have header TECHNICAL" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "TECHNICAL"}, "There is no header for technical"
      end
    end
  end

  test "show.html.erb table must have header SECTION MILITARY ORGANIZATION" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "SECTION MILITARY ORGANIZATION"}, "There is no header for section military organization"
      end
    end
  end

  test "show.html.erb table must have header OPTIONS" do
    @so= So.first
    get :show, id: @so

    assert_select 'table' do
      assert_select 'tr' do
        assert_select 'th', {count: 1, text: "OPTIONS"}, "There is no header for option"
      end
    end
  end

  test "show.html.erb must contain a link to create a new sub service order" do
    @so = So.first
    get :show, id: @so

    assert_select "[href=?]", new_so_sub_service_order_path(@so)
  end

  test "show.html.erb must contain a link to add new materials" do
    @so = So.first

    get :show, id: @so

    @sub_service_orders = @so.sub_service_orders

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td" do
              assert_select "[id=?]", "sub_service_order_add_material_"+sub_service_order.id.to_s, {count: 1} do
                assert_select "[href=?]", new_so_sub_service_order_material_path(@so, sub_service_order)
              end
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a link to show sub service order" do
    @so = So.first
    get :show, id: @so

    @sub_service_orders = @so.sub_service_orders

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td" do
              assert_select "[id=?]", "sub_service_order_show_"+sub_service_order.id.to_s, {count: 1} do
                assert_select "[href=?]", so_sub_service_order_path(@so, sub_service_order), {count: 1, text: "Show"}
              end
            end
          end
        end
      end
    end
  end
  
  test "show.html.erb must contain a link to edit sub service order" do
    @so = So.first
    get :show, id: @so

    @sub_service_orders = @so.sub_service_orders

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td" do
              assert_select "[id=?]", "sub_service_order_edit_"+sub_service_order.id.to_s, {count: 1} do
                assert_select "[href=?]", edit_so_sub_service_order_path(@so, sub_service_order), {count: 1}
              end
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a link to destroy sub service order" do
    @so = So.first
    get :show, id: @so

    @sub_service_orders = @so.sub_service_orders

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td" do
              assert_select "[id=?]", "sub_service_order_destroy_"+sub_service_order.id.to_s, {count: 1} do
                assert_select "[href=?]", so_sub_service_order_path(@so, sub_service_order) do
                  assert_select "[data-method=?]", "delete", {count: 1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a link to list all sos" do
    @so = So.first
    get :show, id: @so

    assert_select "[href=?]", sos_path
  end

  test "show.html.erb must contain a column listing the description of each sub service order" do
    @so = So.first
    get :show, id:@so

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td", {text: sub_service_order.description} do
              assert_select "[id=?]", "sub_service_order_description_"+sub_service_order.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a column listing the amount of each sub service order" do
    @so = So.first
    get :show, id:@so

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td", {text: sub_service_order.amount} do
              assert_select "[id=?]", "sub_service_order_amount_"+sub_service_order.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a column listing the technical of each sub service order" do
    @so = So.first
    get :show, id:@so

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td", {text: sub_service_order.technical} do
              assert_select "[id=?]", "sub_service_order_technical_"+sub_service_order.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "show.html.erb must contain a column listing the sub service order acronym of each sub service order" do
    @so = So.first
    get :show, id:@so

    assert_select "table" do
      assert_select "[id=?]", "sub_service_orders" do
        assert_select "tr" do 
          @so.sub_service_orders.each do |sub_service_order|
            assert_select "td", {text: sub_service_order.section_military_organization.acronym} do
              assert_select "[id=?]", "sub_service_order_section_military_organization_acronym_"+sub_service_order.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end


  ###############
  #edit.html.erb#
  ###############

  test "edit.html.erb must have a h2" do
    @so = build(:so)
    @so.save

    get :edit, id: @so
    
    assert_select 'h2', 1
  end

  test "edit.html.erb h2 must have header Editing - Service Order" do
    @so = build(:so)
    @so.save
    
    get :edit, id: @so

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Editing - Service Order"}, "There is no header for edit service order"
      end
  end

  test "edit.html.erb must contain exactly one form" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form", 1
  end

  test "edit.html.erb must contain exactly one table" do
    @so = build(:so)
    @so.save
    
    get :edit, id: @so

    assert_select "table", 1
  end

  test "edit.html.erb must contain exactly one table with id table_edit_so" do
    @so = build(:so)
    @so.save
    
    get :edit, id: @so

    assert_select "table" do
      assert_select "[id=?]", "table_edit_so"
    end  
  end

  test "edit.html.erb's form must have action attribute with value /sos/id" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "[action=?]", so_path(@so)
    end
  end

  test "edit.html.erb's form must have one submit button" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "edit.html.erb's form must have one label for material_description textfield" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_description" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_material_description", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for material_description" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_description" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[material_description]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for guide number textfield" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_guide_number", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for guide number" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "guide_number" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[guide_number]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for sender name textfield" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_name" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_sender_name", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for sender name" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_name" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[sender_name]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for sender phone textfield" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_phone" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_sender_phone", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for sender phone" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "sender_phone" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[sender_phone]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for total amount textfield" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "total_amount" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_total_amount", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for total amount" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "total_amount" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "so[total_amount]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for material_class_id select" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_material_class_id", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one select for material_class_id" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[material_class_id]", {count:1}
                end 
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one select showing material_class_id correcty" do
    @so = So.first

    get :edit, id: @so

    @material_classes = MaterialClass.all

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[material_class_id]", {count:1} do
                    @material_classes.each do |material_class|
                      assert_select "option" do
                        assert_select "[value=?]", material_class.id, {count:1}
                      end  
                    end
                  end                
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one select showing material_class name correcty" do
    @so = So.first

    get :edit, id: @so

    @material_classes = MaterialClass.all

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "material_class" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[material_class_id]", {count:1} do
                    @material_classes.each do |material_class|
                      assert_select "option", {count: 1, text: material_class.name}, "There is no show for edit name material class"
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for military_organization_id" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "so_military_organization_id", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one select for military_organization_id" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[military_organization_id]", {count:1}
                end 
              end
            end
          end
        end
      end
    end 
  end

  test "edit.html.erb's form must have one select showing military_organization_id correcty" do
    @so = So.first

    get :edit, id: @so

    @military_organizations = MilitaryOrganization.all

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "select" do
                    assert_select "[name=?]", "so[military_organization_id]", {count:1} do
                      @military_organizations.each do |military_organization|
                        assert_select "option" do
                          assert_select "[value=?]", military_organization.id, {count:1}
                        end  
                      end
                    end
                end
              end
            end
          end
        end
      end
    end

  end

  test "edit.html.erb's form must have one select showing military organization name correcty" do
    @so = So.first

    get :edit, id: @so

    @military_organizations = MilitaryOrganization.all

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_so" do
          assert_select "tr" do
            assert_select "[id=?]", "military_organization" do
              assert_select "td" do
                assert_select "select" do
                  assert_select "[name=?]", "so[military_organization_id]", {count:1} do
                    @military_organizations.each do |military_organization|
                      assert_select "option", {count: 1, text: military_organization.name}, "There is no show for edit name military organization"
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end


  test "edit.html.erb must contain a link to list all materials" do
    @so = build(:so)
    @so.save

    get :edit, id: @so

    assert_select "[href=?]", sos_path
  end

end
