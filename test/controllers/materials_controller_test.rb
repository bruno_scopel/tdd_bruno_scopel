require 'test_helper'

class MaterialsControllerTest < ActionController::TestCase

  ###################
  #Tests for actions#
  ###################

  def setup
    create(:material)
  
    @so = So.first
    @sub_service_order = SubServiceOrder.first
    @material = Material.first
  end

  #####
  #new#
  #####

  test "should be possible request new action" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_response :success, "should be possible to request new action"
  end

  test "action new should create an instance variable" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_not_nil assigns(:material), "action new does not create an instance variable of type Material"
  end

  test "after calling the new action passing an invalid id the so, you must be redirected to the action index the so " do
    get :new, so_id: -1, sub_service_order_id: @sub_service_order

    assert_redirected_to sos_path
  end

  test "after calling the new action passing an invalid id the sub_service_order, you must be redirected to the action show the so " do
    get :new, so_id: @so, sub_service_order_id: -1

    assert_redirected_to so_path(@so)
  end

  ########
  #create#
  ########

  test "action create really creates a new well-formed Material based on the form" do
    
    assert_difference('Material.count') do
      post :create, so_id: @so, sub_service_order_id: @sub_service_order, material: {name: "Name", amount: 10.0, value: 10.0, unit: "Unit" , sub_service_order_id: @sub_service_order}
    end
  end

  test "action create redirects to action show the sub_service_order after creating a well-formed Material based on the form" do
    post :create, so_id: @so, sub_service_order_id: @sub_service_order, material: {name: "Name", amount: 10.0, value: 10.0, unit: "Unit" , sub_service_order_id: @sub_service_order}

    assert_redirected_to so_sub_service_order_path(@so, @sub_service_order)
  end

  test "action create redirects to action new after creating a badly formed Material based on the form" do
    post :create, so_id: @so, sub_service_order_id: @sub_service_order, material: {name: "Empty"}

    assert_template :new
  end

  test "after a badly formed submission, fields filled correcty must be displayed" do
    post :create, so_id: @so, sub_service_order_id: @sub_service_order, material: {name: "Empty"}

    assert_equal assigns(:material).name, "Empty"
  end

  test "after calling the create action passing an invalid id the so, you must be redirected to the action index the so " do
    post :create, so_id: -1, sub_service_order_id: @sub_service_order, material: {name: "Name", amount: 10.0, value: 10.0, unit: "Unit" , sub_service_order_id: @sub_service_order}

    assert_redirected_to sos_path
  end

  test "after calling the create action passing an invalid id the sub_service_order, you must be redirected to the action show the so " do
    post :create, so_id: @so, sub_service_order_id: -1, material: {name: "Name", amount: 10.0, value: 10.0, unit: "Unit" , sub_service_order_id: @sub_service_order}

    assert_redirected_to so_path(@so)
  end

  #########
  #destroy#
  #########

  test "destroy should delete a valid tuple" do
 
    assert_difference('Material.count', -1) do
      delete :destroy, so_id: @so, sub_service_order_id: @sub_service_order, id: @material
    end
  end

  test "after calling the destroy action passing an invalid id, you must be redirected to the action show the sub service order" do
    delete :destroy, so_id: @so, sub_service_order_id: @sub_service_order, id: -1

    assert_redirected_to so_sub_service_order_path(@so, @sub_service_order)
  end

  test "after calling the destroy action passing an invalid id the so, you must be redirected to the action index the so " do
    delete :destroy, so_id: -1, sub_service_order_id: @sub_service_order, id: @material

    assert_redirected_to sos_path
  end

  test "after calling the destroy action passing an invalid id the sub service order, you must be redirected to the action show the so" do
    delete :destroy, so_id: @so, sub_service_order_id: -1, id: @material

    assert_redirected_to so_path(@so)
  end

  #################
  #Tests for views#
  #################

  ################
  #new.html.erb#
  ################

  test "new.html.erb must have a h2" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select 'h2', 1
  end

  test "new.html.erb h2 must have header Creating a new material" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Creating a new material"}, "There is no header for material"
      end
  end

  test "new.html.erb must contain exactly one form" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form", 1
  end

  test "new.html.erb must contain exactly one table" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "table", 1
  end

  test "new.html.erb must contain exactly one table with id equal material" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "table" do
      assert_select "[id=?]", "material"
    end  
  end

  test "new.html.erb's form must have action attribute with value /materials" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "[action=?]", so_sub_service_order_materials_path(@so, @sub_service_order)
    end
  end

  test "new.html.erb's form must have an attribute method with value post" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "[method=?]", "post"
    end
  end

  test "new.html.erb's form must have one submit button" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "new.html.erb's form must have one label for name" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "material_name", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for name" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "material[name]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for amount textfield" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "amount" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "material_amount", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for amount" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "amount" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "material[amount]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for value textfield" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "value" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "material_value", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for value" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "value" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "material[value]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for unit textfield" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "unit" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "material_unit", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for unit" do
   get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "unit" do
              assert_select "td" do
                assert_select "input" do
                  assert_select "[type=?]", "text" do
                    assert_select "[name=?]", "material[unit]", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for sub service order description" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "sso_description" do
              assert_select "td" do
                assert_select "label" do
                  assert_select "[for=?]", "material_sub_service_order_description", {count:1}
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one td for sub service order description" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "sso_description" do
              assert_select "td", {text:@sub_service_order.description, count: 1}
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have a hidden field with sub_service_order_id" do
    get :new, so_id: @so, sub_service_order_id: @sub_service_order

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "material" do
          assert_select "tr" do
            assert_select "[id=?]", "sso_description" do
              assert_select "input" do
                assert_select "[type=?]", "hidden" do
                  assert_select "[value=?]", @sub_service_order.id, {count: 1}
                end
              end
            end
          end
        end
      end
    end
  end
end