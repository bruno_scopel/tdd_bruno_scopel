require 'test_helper'

class MilitaryOrganizationsControllerTest < ActionController::TestCase

  ###################
  #Tests for actions#
  ###################

  def setup
    create(:military_organization)
  end

  #######
  #index#
  #######
  test "should be possible request index action" do
  	get :index

  	assert_response :success, "should be possible to request index action"
  end

  test "action index should retrieve all military organization" do

    get :index
    
    assert assigns(:military_organizations) == MilitaryOrganization.all, "military_organization local variable does not contain all military organization"
  end

  #####
  #new#
  #####

  test "should be possible request new action" do
    get :new

    assert_response :success, "should be possible to request new action"
  end

  test "action new should create an instance variable" do
    get :new

    assert_not_nil assigns(:military_organization), "action new does not create an instance variable of type Military Organization"
  end

  ########
  #create#
  ########

  test "action create really creates a new well-formed Military Organization based on the form" do
    assert_difference('MilitaryOrganization.count') do
      post :create, military_organization: {name: "Name", acronym: "Acronym"}
    end
  end

  test "action create redirects to action show after creating a well-formed MilitaryOrganization based on the form" do
    post :create, military_organization: {name: "Name", acronym: "Acronym"}

    assert_redirected_to military_organizations_path
  end

  test "action create redirects to action new after creating a badly formed Military Organization based on the form" do
    post :create, military_organization: {acronym: "Acronym"}

    assert_template :new
  end

  test "after a badly formed submission, fields filled correcty must be displayed" do
    post :create, military_organization: {acronym: "Acronym"}

    assert_equal assigns(:military_organization).acronym, "Acronym"
  end

  #########
  #destroy#
  #########

  test "destroy should delete a valid tuple" do
    @military_organization = build(:military_organization)
    @military_organization.save
    
    assert_difference('MilitaryOrganization.count', -1) do
      delete :destroy, id: @military_organization
    end
  end

  test "after calling the destroy action passing an invalid id, you must be redirected to the action index" do
    delete :destroy, id: -1

    assert_redirected_to military_organizations_path
  end

  ##########
  # edit #
  ##########

  test "should get edit" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization
    assert_response :success
  end

  test "after calling edit action passing a valid id, you must be redirected to the view edit.html.erb" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_template :edit
  end

  test "after calling the edit action passing an invalid id, you must be redirected to the action index" do
    get :edit, id: -1

    assert_redirected_to military_organizations_path
  end

  test "after calling the edit action, there must be a local variable storing the military_organization retrieved from database" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_not_nil assigns(:military_organization)
  end

  test "after calling the edit action, there must be a coherent local variable storing the military_organization retrieved from database" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_equal @military_organization, assigns(:military_organization)
  end

  
  ##########
  # update #
  ##########

  test "should redirect to index of military organization after updating a valid military_organization" do
    @military_organization = build(:military_organization)
    @military_organization.save

    patch :update, id: @military_organization, military_organization: { name: @military_organization.name, acronym: @military_organization.acronym }
    assert_redirected_to military_organizations_path
  end

  test "should update a valid military_organization correcty" do
    @military_organization = build(:military_organization)
    @military_organization.save

    @military_organization.name = "My new name"
    @military_organization.acronym = "My new acronym"

    patch :update, id: @military_organization, military_organization: { name: @military_organization.name, acronym: @military_organization.acronym }

    @military_organization_from_database = MilitaryOrganization.find(@military_organization.id)

    assert_equal @military_organization_from_database.name, @military_organization.name
    assert_equal @military_organization_from_database.acronym, @military_organization.acronym
  end

  test "update should redirect to index after receiving an invalid id" do
    patch :update, id: -1, military_organization: {}
    assert_redirected_to military_organizations_path
  end

  test "update should return to edit.html.erb after trying to update an invalid military_organization" do
    @military_organization = build(:military_organization)
    @military_organization.save

    @military_organization.name = nil

    patch :update, id: @military_organization, military_organization: { name: @military_organization.name }

    assert_template :edit
  end


  ###################
  #Tests for views###
  ###################

  ################
  #index.html.erb#
  ################
  test "index.html.erb must have a h2" do
    get :index

    assert_select 'h2', 1
  end

  test "index.html.erb h2 must have header MILITARY ORGANIZATIONS" do
    get :index

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "MILITARY ORGANIZATIONS"}, "There is no header for military organizations"
      end
  end
  test "index.html.erb must have a table" do
    get :index

    assert_select 'table', 1
  end

  test "index.html.erb must contain exactly one table with id military_organizations" do
    get :index

    assert_select "table" do
      assert_select "[id=?]", "military_organizations"
    end  
  end

  test "index.html.erb table must have tree headers" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "headers" do
            assert_select 'th', 3
          end
        end
      end
    end
  end

  test "index.html.erb table must have header NAME" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "headers" do
            assert_select 'th', {count: 1, text: "NAME"}, "There is no header for name"
          end
        end
      end
    end
  end

   test "index.html.erb table must have header ACRONYM" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "headers" do
            assert_select 'th', {count: 1, text: "ACRONYM"}, "There is no header for acronym"
          end
        end
      end
    end
  end

  test "index.html.erb table must have header OPTIONS" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "headers" do
            assert_select 'th', {count: 1, text: "OPTIONS"}, "There is no header for OPTIONS"
          end
        end
      end
    end
  end

  test "field name of military organizationes is being displayed on table" do
    get :index
    
    @military_organizations = MilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "military_organizations" do
        assert_select "tr" do 
          @military_organizations.each do |military_organization|
            assert_select "td", {text: military_organization.name} do
              assert_select "[id=?]", "military_organization_name_"+military_organization.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "field acronym of military organization is being displayed on table" do
    get :index
    
    @military_organizations = MilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "military_organizations" do
        assert_select "tr" do 
          @military_organizations.each do |military_organization|
            assert_select "td", {text: military_organization.acronym} do
              assert_select "[id=?]", "military_organization_acronym_"+military_organization.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to edit military organization" do
    get :index

    @military_organizations = MilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "military_organizations" do
        assert_select "tr" do 
          @military_organizations.each do |military_organization|
            assert_select "td" do
              assert_select "[id=?]", "military_organization_edit_"+military_organization.id.to_s, {count: 1} do
                assert_select "[href=?]", edit_military_organization_path(military_organization), {count: 1}
              end
            end
          end
        end
      end
    end
  end
  
  test "index.html.erb must contain a link to destroy a military organization" do
    get :index

    @military_organizations = MilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "military_organizations" do
        assert_select "tr" do
          @military_organizations.each do |military_organization|
            assert_select "td" do
              assert_select "[id=?]", "military_organization_destroy_"+military_organization.id.to_s, {count: 1} do
                assert_select "[href=?]", military_organization_path(military_organization) do
                  assert_select "[data-method=?]", "delete"
                end
              end
            end            
          end
        end
      end
    end
  end
 
  test "index.html.erb must contain a link to create a new military organization" do
    get :index

    assert_select "[href=?]", new_military_organization_path
  end

  test "index.html.erb must contain a link to sos" do
    get :index

    assert_select "[href=?]", sos_path
  end

  ################
  #new.html.erb#
  ################


  test "new.html.erb must have a h2" do
    get :new

    assert_select 'h2', 1
  end

  test "new.html.erb h2 must have header Creating a new military organization" do
    get :new

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Creating a new military organization"}, "There is no header for military organization"
      end
  end

  test "new.html.erb must contain exactly one form" do
    get :new

    assert_select "form", 1
  end

  test "new.html.erb must contain exactly one table" do
    get :new

    assert_select "table", 1
  end

  test "new.html.erb must contain exactly one table with id table_new_military_organization" do
    get :new

    assert_select "table" do
      assert_select "[id=?]", "table_new_military_organization"
    end  
  end
  
  test "new.html.erb's form must have action attribute with value /military_organization" do
    get :new

    assert_select "form" do
      assert_select "[action=?]", "/military_organizations"
    end
  end

  test "new.html.erb's form must have an attribute method with value post" do
    get :new

    assert_select "form" do
      assert_select "[method=?]", "post"
    end
  end

  test "new.html.erb's form must have one submit button" do
    get :new

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "new.html.erb's form must have one label for name textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "military_organization_name", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for name" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "military_organization[name]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for acronym textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "military_organization_acronym", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for acronym" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_new_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "military_organization[acronym]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb must contain a link to list all military organizations" do
    get :new

    assert_select "[href=?]", military_organizations_path
  end

  ###############
  #edit.html.erb#
  ###############

  test "edit.html.erb must have a h2" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select 'h2', 1
  end

  test "edit.html.erb h2 must have header Editing - military organization" do
    @military_organization = build(:military_organization)
    @military_organization.save
    
    get :edit, id: @military_organization

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Editing - military organization"}, "There is no header for edit military organization"
      end
  end

  test "edit.html.erb must contain exactly one form" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form", 1
  end

  test "edit.html.erb must contain exactly one table" do
    @military_organization = build(:military_organization)
    @military_organization.save
    
    get :edit, id: @military_organization

    assert_select "table", 1
  end

  test "edit.html.erb must contain exactly one table with id table_edit_military_organization" do
    @military_organization = build(:military_organization)
    @military_organization.save
    
    get :edit, id: @military_organization

    assert_select "table" do
      assert_select "[id=?]", "table_edit_military_organization"
    end  
  end

  test "edit.html.erb's form must have action attribute with value /military_organization/id" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form" do
      assert_select "[action=?]", military_organization_path(@military_organization)
    end
  end

  test "edit.html.erb's form must have one submit button" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "edit.html.erb's form must have one label for name textfield" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "military_organization_name", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for name" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "military_organization[name]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for acronym textfield" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "military_organization_acronym", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for acronym" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "table_edit_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "military_organization[acronym]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb must contain a link to list all military organization" do
    @military_organization = build(:military_organization)
    @military_organization.save

    get :edit, id: @military_organization

    assert_select "[href=?]", military_organizations_path
  end
  
end