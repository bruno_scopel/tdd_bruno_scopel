require 'test_helper'

class SectionMilitaryOrganizationsControllerTest < ActionController::TestCase

  ###################
  #Tests for actions#
  ###################

  def setup
    create(:section_military_organization)
  end

  #######
  #index#
  #######
  test "should be possible request index action" do
  	get :index

  	assert_response :success, "should be possible to request index action"
  end

  test "action index should retrieve all section military organization" do

    get :index

    assert assigns(:section_military_organizations) == SectionMilitaryOrganization.all, "section_military_organizations local variable does not contain all section military organization"
  end

  #####
  #new#
  #####

  test "should be possible request new action" do
    get :new

    assert_response :success, "should be possible to request new action"
  end

  test "action new should create an instance variable" do
    get :new

    assert_not_nil assigns(:section_military_organization), "action new does not create an instance variable of type SectionMilitaryOrganization"
  end

  ########
  #create#
  ########

  test "action create really creates a new well-formed SectionMilitaryOrganization based on the form" do
    assert_difference('SectionMilitaryOrganization.count') do
      post :create, section_military_organization: {name: "Name", acronym: "Acronym"}
    end
  end

  test "action create redirects to action show after creating a well-formed section_military_organization based on the form" do
    post :create, section_military_organization: {name: "Name", acronym: "Acronym"}

    assert_redirected_to section_military_organizations_path
  end

  test "action create redirects to action new after creating a badly formed section_military_organization based on the form" do
    post :create, section_military_organization: {acronym: "Acronym"}

    assert_template :new
  end

  test "after a badly formed submission, fields filled correcty must be displayed" do
    post :create, section_military_organization: {acronym: "Acronym"}

    assert_equal assigns(:section_military_organization).acronym, "Acronym"
  end

  #########
  #destroy#
  #########

  test "destroy should delete a valid tuple" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save
    
    assert_difference('SectionMilitaryOrganization.count', -1) do
      delete :destroy, id: @section_military_organization
    end
  end

  test "after calling the destroy action passing an invalid id, you must be redirected to the action index" do
    delete :destroy, id: -1

    assert_redirected_to section_military_organizations_path
  end

  ##########
  # edit   #
  ##########

  test "should get edit" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization
    assert_response :success
  end

  test "after calling edit action passing a valid id, you must be redirected to the view edit.html.erb" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_template :edit
  end

  test "after calling the edit action passing an invalid id, you must be redirected to the action index" do
    get :edit, id: -1

    assert_redirected_to section_military_organizations_path
  end

  test "after calling the edit action, there must be a local variable storing the section military organization retrieved from database" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_not_nil assigns(:section_military_organization)
  end

  test "after calling the edit action, there must be a coherent local variable storing the section_military_organization retrieved from database" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_equal @section_military_organization, assigns(:section_military_organization)
  end

  ##########
  # update #
  ##########

  test "should redirect to action show after updating a valid section military organization" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    patch :update, id: @section_military_organization, section_military_organization: { name: @section_military_organization.name, acronym: @section_military_organization.acronym }
    assert_redirected_to section_military_organizations_path
  end

  test "should update a valid section_military_organization correcty" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    @section_military_organization.name = "My new name"
    @section_military_organization.acronym = "My new acronym"

    patch :update, id: @section_military_organization, section_military_organization: { name: @section_military_organization.name, acronym: @section_military_organization.acronym }

    @section_military_organization_from_database = SectionMilitaryOrganization.find(@section_military_organization.id)

    assert_equal @section_military_organization_from_database.name, @section_military_organization.name
    assert_equal @section_military_organization_from_database.acronym, @section_military_organization.acronym
  end

  test "update should redirect to index after receiving an invalid id" do
    patch :update, id: -1, section_military_organization: {}
    assert_redirected_to section_military_organizations_path
  end

  test "update should return to edit.html.erb after trying to update an invalid section_military_organization" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    @section_military_organization.name = nil

    patch :update, id: @section_military_organization, section_military_organization: { name: @section_military_organization.name }

    assert_template :edit
  end

  ###################
  #Tests for views###
  ###################

  ################
  #index.html.erb#
  ################
  test "index.html.erb must have a h2" do
    get :index

    assert_select 'h2', 1
  end

  test "index.html.erb h2 must have header SECTION MILITARY ORGANIZATIONS" do
    get :index

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "SECTION MILITARY ORGANIZATIONS"}, "There is no header for section military organizations"
      end
  end

  test "index.html.erb must have a table" do
    get :index

    assert_select 'table', 1
  end

  test "index.html.erb must contain exactly one table with id section_military_organizations" do
    get :index

    assert_select "table" do
      assert_select "[id=?]", "section_military_organizations"
    end  
  end

  test "index.html.erb table must have two headers" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', 3
          end
        end
      end
    end
  end

  test "index.html.erb table must have header NAME" do
    get :index

    assert_select 'table' do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "NAME"}, "There is no header for name"
          end
        end
      end
    end
  end

   test "index.html.erb table must have header ACRONYM" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "ACRONYM"}, "There is no header for acronym"
          end
        end
      end
    end
  end

  test "index.html.erb table must have header OPTIONS" do
    get :index
    
    assert_select 'table' do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select 'tr' do
          assert_select "[id=?]", "header" do
            assert_select 'th', {count: 1, text: "OPTIONS"}, "There is no header for OPTIONS"
          end
        end
      end
    end
  end

  test "field name of section military organization is being displayed on table" do
    get :index

    @section_military_organizations = SectionMilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select "tr" do 
          @section_military_organizations.each do |section_military_organization|
            assert_select "td", {text: section_military_organization.name} do
              assert_select "[id=?]", "section_military_organization_name_"+section_military_organization.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "field acronym of section_military_organizations is being displayed on table" do
    get :index

    @section_military_organizations = SectionMilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select "tr" do 
          @section_military_organizations.each do |section_military_organization|
            assert_select "td", {text: section_military_organization.acronym} do
              assert_select "[id=?]", "section_military_organization_acronym_"+section_military_organization.id.to_s, {count: 1}
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to edit section_military_organization" do
    get :index

    @section_military_organizations = SectionMilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select "tr" do
          @section_military_organizations.each do |section_military_organization|
            assert_select "td" do
              assert_select "[id=?]", "section_military_organization_edit_"+section_military_organization.id.to_s, {count: 1} do
                assert_select "[href=?]", edit_section_military_organization_path(section_military_organization), {count: 1}
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to create a new section_military_organization" do
    get :index

    assert_select "[href=?]", new_section_military_organization_path
  end

  test "index.html.erb must contain a link to destroy a section military organization" do
    get :index

    @section_military_organizations = SectionMilitaryOrganization.all

    assert_select "table" do
      assert_select "[id=?]", "section_military_organizations" do
        assert_select "tr" do
          @section_military_organizations.each do |section_military_organization|
            assert_select "td" do
              assert_select "[id=?]", "section_military_organization_destroy_"+section_military_organization.id.to_s, {count: 1} do
                assert_select "[href=?]", section_military_organization_path(section_military_organization) do
                  assert_select "[data-method=?]", "delete"
                end
              end
            end
          end
        end
      end
    end
  end

  test "index.html.erb must contain a link to sos (show)" do
    get :index

    assert_select "[href=?]", sos_path
  end

  ################
  #new.html.erb#
  ################

  test "new.html.erb must have a h2" do
    get :new

    assert_select 'h2', 1
  end

  test "new.html.erb h2 must have header Creating a new section military organization" do
    get :new

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Creating a new section military organization"}, "There is no header for military organization"
      end
  end

  test "new.html.erb must contain exactly one form" do
    get :new

    assert_select "form", 1
  end

  test "new.html.erb must contain exactly one table" do
    get :new

    assert_select "table", 1
  end
  test "new.html.erb must contain exactly one table with id section_military_organization" do
    get :new

    assert_select "table" do
      assert_select "[id=?]", "section_military_organization"
    end  
  end

  test "new.html.erb's form must have action attribute with value /section_military_organization" do
    get :new

    assert_select "form" do
      assert_select "[action=?]", "/section_military_organizations"
    end
  end

  test "new.html.erb's form must have an attribute method with value post" do
    get :new

    assert_select "form" do
      assert_select "[method=?]", "post"
    end
  end

  test "new.html.erb's form must have one submit button" do
    get :new

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "new.html.erb's form must have one label for name textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "section_military_organization_name", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for name" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "section_military_organization[name]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one label for acronym textfield" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "section_military_organization_acronym", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb's form must have one textfield for acronym" do
    get :new

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "section_military_organization[acronym]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "new.html.erb must contain a link to list all section_military_organization" do
    get :new

    assert_select "[href=?]", section_military_organizations_path
  end

  ###############
  #edit.html.erb#
  ###############

  test "edit.html.erb must have a h2" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select 'h2', 1
  end

  test "edit.html.erb h2 must have header Editing - section military organization" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save
    
    get :edit, id: @section_military_organization

    assert_select 'h2' do
        assert_select 'b', {count: 1, text: "Editing - section military organization"}, "There is no header for edit section military organization"
      end
  end

  test "edit.html.erb must contain exactly one form" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form", 1
  end

  test "edit.html.erb must contain exactly one table" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save
    
    get :edit, id: @section_military_organization

    assert_select "table", 1
  end

  test "edit.html.erb must contain exactly one table with id section_military_organization" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save
    
    get :edit, id: @section_military_organization

    assert_select "table" do
      assert_select "[id=?]", "section_military_organization"
    end  
  end

  test "edit.html.erb's form must have action attribute with value /section_military_organization/id" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form" do
      assert_select "[action=?]", section_military_organization_path(@section_military_organization)
    end
  end

  test "edit.html.erb's form must have one submit button" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form" do
      assert_select "[type=?]", "submit", {count:1}
    end
  end

  test "edit.html.erb's form must have one label for name" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "section_military_organization_name", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for name" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "name" do
              assert_select "td" do
                assert_select "[id=?]", "name_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "section_military_organization[name]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one label for acronym" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_label" do
                  assert_select "label" do
                    assert_select "[for=?]", "section_military_organization_acronym", {count:1}
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb's form must have one textfield for acronym" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "form" do
      assert_select "table" do
        assert_select "[id=?]", "section_military_organization" do
          assert_select "tr" do
            assert_select "[id=?]", "acronym" do
              assert_select "td" do
                assert_select "[id=?]", "acronym_textfield" do
                  assert_select "input" do
                    assert_select "[type=?]", "text" do
                      assert_select "[name=?]", "section_military_organization[acronym]", {count:1}
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  test "edit.html.erb must contain a link to list all section military organization" do
    @section_military_organization = build(:section_military_organization)
    @section_military_organization.save

    get :edit, id: @section_military_organization

    assert_select "[href=?]", section_military_organizations_path
  end
end